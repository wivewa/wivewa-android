# wivewa-android

This is the Android client for [WiVeWa](https://www.wivewa.de/).
It is mainly a Browser that opens WiVeWa, but there is more:

- the App uses a private key that is generated and stored in your hardware for the authentication
- the App downloads the addressbook and provides it as "Wontakte"
- the App can show caller details (name + last ticket) when using [Welefon](https://codeberg.org/wivewa/wivewa-dialer-android) or another compatible App
- the App can upload incoming and outgoing call details to the WiVeWa phone system ("Korrespondenzliste" extension)

## Download

- <https://f-droid.org/packages/de.wivewa.android/>
- <https://codeberg.org/wivewa/wivewa-android-apk>

The APK files are (should be) byte for byte identical because F-Droid checks
the APK files from the second source and uses them after that.

F-Droid provides simple updates using the F-Droid client while the direct
download provides the newest release without delay.

## License

```
wivewa-android - the Android client for the WiVeWa CRM
Copyright (C) 2023 Jonas Lochmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see https://www.gnu.org/licenses/.
```

Exception: The WiVeWa name and logo are proprietary
