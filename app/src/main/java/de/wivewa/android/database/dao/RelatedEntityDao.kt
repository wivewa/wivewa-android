/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.wivewa.android.database.model.RelatedEntity
import de.wivewa.android.database.model.join.RelatedEntityWithName
import kotlinx.coroutines.flow.Flow

@Dao
interface RelatedEntityDao {
    @Query("DELETE FROM related_entity")
    fun deleteAll()

    @Insert
    fun insert(items: List<RelatedEntity>)

    @Query("SELECT entity.id AS entity2_id, entity.computed_display_name AS entity2_name, entity.type AS entity2_type FROM entity JOIN related_entity WHERE related_entity.entity1_id = :id AND entity.id = related_entity.entity2_id OR related_entity.entity2_id = :id AND entity.id = related_entity.entity1_id ORDER BY LOWER(entity2_name)")
    fun getByIdFlow(id: Long): Flow<List<RelatedEntityWithName>>
}