/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.*
import de.wivewa.android.database.model.Customer
import kotlinx.coroutines.flow.Flow

@Dao
interface CustomerInfoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIgnoreConflict(info: Customer)

    @Update
    fun update(info: Customer)

    @Transaction
    fun updateOrInsert(info: Customer) { update(info); insertIgnoreConflict(info) }

    @Query("DELETE FROM customer WHERE id NOT IN (:currentIds)")
    fun deleteOtherIds(currentIds: List<Long>)

    @Query("SELECT COUNT(*) FROM customer")
    fun count(): Long

    @Query("SELECT * FROM customer WHERE EXISTS (SELECT * FROM customer_role cr WHERE cr.customer_id = customer.id AND cr.role IN (:roles))")
    fun selectAllWithRoleLive(roles: List<String>): Flow<List<Customer>>
}