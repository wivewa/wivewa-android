/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database

import androidx.room.AutoMigration
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.wivewa.android.database.converter.ComponentNameConverter
import de.wivewa.android.database.converter.LocalDateConverter
import de.wivewa.android.database.dao.*
import de.wivewa.android.database.model.*

@androidx.room.Database(
    version = 6,
    entities = [
        ConfigurationItem::class,
        Customer::class,
        CustomerRole::class,
        LoggedPhoneCall::class,
        ContactType::class,
        Salutation::class,
        EntityGroup::class,
        Entity::class,
        Address::class,
        ContactInfo::class,
        RelatedEntity::class,
        PhoneAccountUploadConfiguration::class
    ],
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
        AutoMigration(from = 4, to = 5),
        AutoMigration(from = 5, to = 6)
    ]
)
@TypeConverters(
    LocalDateConverter::class,
    ComponentNameConverter::class
)
abstract class Database: RoomDatabase() {
    abstract val configuration: ConfigurationDao
    abstract val customer: CustomerInfoDao
    abstract val customerRole: CustomerRoleDao
    abstract val loggedPhoneCall: LoggedPhoneCallDao
    abstract val contactType: ContactTypeDao
    abstract val salutation: SalutationDao
    abstract val entityGroup: EntityGroupDao
    abstract val entity: EntityDao
    abstract val address: AddressDao
    abstract val contactInfo: ContactInfoDao
    abstract val relatedEntity: RelatedEntityDao
    abstract val phoneAccountUploadConfiguration: PhoneAccountUploadConfigurationDao
}