/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import de.wivewa.android.extensions.nullableString

@Entity(
    tableName = "contact_type"
)
data class ContactType (
    @PrimaryKey
    val id: Long,
    val name: String,
    @ColumnInfo(name = "uri_schema")
    val uriSchema: String?
) {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
        private const val URI_SCHEMA = "uri_schema"

        fun fromJson(reader: JsonReader): ContactType {
            var id: Long? = null
            var name: String? = null
            var uriSchema: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextLong()
                    NAME -> name = reader.nextString()
                    URI_SCHEMA -> uriSchema = reader.nullableString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ContactType(
                id = id!!,
                name = name!!,
                uriSchema = uriSchema
            )
        }
    }

    object UriSchema {
        const val TEL = "tel"
        const val FAX = "fax"
        const val MAILTO = "mailto"
        private const val HTTP = "http"
        private const val HTTPS = "https"
        val WEB = setOf(HTTP, HTTPS)
        const val XMPP = "xmpp"
    }
}