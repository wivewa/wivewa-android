/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import de.wivewa.android.extensions.nullableLong

@androidx.room.Entity(
    tableName = "contact_info",
    foreignKeys = [
        ForeignKey(
            entity = Entity::class,
            childColumns = ["entity1_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Entity::class,
            childColumns = ["entity2_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = ContactType::class,
            childColumns = ["contact_type_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class ContactInfo (
    @PrimaryKey
    val id: Long,
    @ColumnInfo(name = "entity1_id", index = true)
    val entityId1: Long,
    @ColumnInfo(name = "entity2_id", index = true)
    val entityId2: Long?,
    @ColumnInfo(name = "contact_type_id", index = true)
    val contactTypeId: Long,
    val type: String,
    val info: String
) {
    companion object {
        const val TYPE_CONTACT = "contact"
        const val TYPE_RELATION = "relation"

        private const val ID = "id"
        private const val ENTITY1 = "entity1"
        private const val ENTITY2 = "entity2"
        private const val CONTACT_TYPE = "contact_type"
        private const val TYPE = "type"
        private const val INFO = "info"

        fun fromJson(reader: JsonReader): ContactInfo {
            var id: Long? = null
            var entity1: Long? = null
            var entity2: Long? = null
            var contactType: Long? = null
            var type: String? = null
            var info: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextLong()
                    ENTITY1 -> entity1 = reader.nextLong()
                    ENTITY2 -> entity2 = reader.nullableLong()
                    CONTACT_TYPE -> contactType = reader.nextLong()
                    TYPE -> type = reader.nextString()
                    INFO -> info = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return ContactInfo(
                id = id!!,
                entityId1 = entity1!!,
                entityId2 = entity2,
                contactTypeId = contactType!!,
                type = type!!,
                info = info!!
            )
        }
    }
}