/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import de.wivewa.android.extensions.nullableLong
import de.wivewa.android.extensions.nullableString
import java.time.LocalDate

@androidx.room.Entity(
    tableName = "entity",
    foreignKeys = [
        ForeignKey(
            entity = Customer::class,
            childColumns = ["owner_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = EntityGroup::class,
            childColumns = ["group_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = Salutation::class,
            childColumns = ["person_salutation_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class Entity (
    @PrimaryKey
    val id: Long,
    @ColumnInfo(name = "owner_id", index = true)
    val ownerId: Long?,
    @ColumnInfo(name = "group_id", index = true)
    val groupId: Long?,
    val type: String,
    // computed
    @ColumnInfo(name = "computed_display_name", index = true)
    val computedDisplayName: String,
    // person only
    @ColumnInfo(name = "person_firstname")
    val personFirstname: String?,
    @ColumnInfo(name = "person_lastname")
    val personLastname: String?,
    @ColumnInfo(name = "person_birthdate")
    val personBirthdate: LocalDate?,
    @ColumnInfo(name = "person_salutation_id", index = true)
    val personSalutationId: Long?,
    @ColumnInfo(name = "person_title")
    val personTitle: String?,
    // organization only
    @ColumnInfo(name = "organization_name")
    val organizationName: String?,
    // usergroup only
    @ColumnInfo(name = "usergroup_name")
    val usergroupName: String?
) {
    sealed class Sealed {
        data class Person(
            val firstname: String,
            val lastname: String,
            val birthdate: LocalDate?,
            val salutationId: Long?,
            val title: String
        ): Sealed()

        data class Organization(
            val name: String
        ): Sealed()

        data class UserGroup(
            val name: String
        ): Sealed()
    }

    @Transient
    val sealed: Sealed = if (type == TYPE_PERSON) {
        if (
            personFirstname == null ||
            personLastname == null ||
            personTitle == null ||
            organizationName != null ||
            usergroupName != null
        ) throw IllegalStateException()

        Sealed.Person(
            firstname = personFirstname,
            lastname = personLastname,
            birthdate = personBirthdate,
            salutationId = personSalutationId,
            title = personTitle
        )
    } else if (type == TYPE_ORGANIZATION) {
        if (
            personFirstname != null ||
            personLastname != null ||
            personBirthdate != null ||
            personSalutationId != null ||
            personTitle != null ||
            organizationName == null ||
            usergroupName != null
        ) throw IllegalStateException()

        Sealed.Organization(name = organizationName)
    } else if (type == TYPE_USERGROUP) {
        if (
            personFirstname != null ||
            personLastname != null ||
            personBirthdate != null ||
            personSalutationId != null ||
            personTitle != null ||
            organizationName != null ||
            usergroupName == null
        ) throw IllegalStateException()

        Sealed.UserGroup(usergroupName)
    } else throw IllegalStateException()

    companion object {
        const val TYPE_PERSON = "person"
        const val TYPE_ORGANIZATION = "organization"
        const val TYPE_USERGROUP = "usergroup"

        private const val ID = "id"
        private const val OWNER = "owner"
        private const val GROUP = "group"
        private const val TYPE = "type"
        private const val FIRSTNAME = "firstname"
        private const val LASTNAME = "lastname"
        private const val BIRTHDATE = "birthdate"
        private const val SALUTATION = "salutation"
        private const val TITLE = "title"
        private const val NAME = "name"

        fun fromJson(reader: JsonReader): Entity {
            var id: Long? = null
            var ownerId: Long? = null
            var groupId: Long? = null
            var type: String? = null
            var personFirstname: String? = null
            var personLastname: String? = null
            var personBirthdate: LocalDate? = null
            var personSalutationId: Long? = null
            var personTitle: String? = null
            var name: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextLong()
                    OWNER -> ownerId = reader.nullableLong()
                    GROUP -> groupId = reader.nullableLong()
                    TYPE -> type = reader.nextString()
                    FIRSTNAME -> personFirstname = reader.nextString()
                    LASTNAME -> personLastname = reader.nextString()
                    BIRTHDATE -> personBirthdate = reader.nullableString()?.let { LocalDate.parse(it) }
                    SALUTATION -> personSalutationId = reader.nullableLong()
                    TITLE -> personTitle = reader.nextString()
                    NAME -> name = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return Entity(
                id = id!!,
                ownerId = ownerId,
                groupId = groupId,
                type = type!!,
                computedDisplayName = when (type) {
                    TYPE_ORGANIZATION -> name
                    TYPE_PERSON ->
                        if (personFirstname != null && personLastname != null) {
                            if (personFirstname.isEmpty()) personLastname
                            else if (personLastname.isEmpty()) personFirstname
                            else "$personFirstname $personLastname"
                        } else null
                    TYPE_USERGROUP -> name
                    else -> null
                } ?: id.toString(),
                personFirstname = personFirstname,
                personLastname = personLastname,
                personBirthdate = personBirthdate,
                personSalutationId = personSalutationId,
                personTitle = personTitle,
                organizationName = if (type == TYPE_ORGANIZATION) name else null,
                usergroupName = if (type == TYPE_USERGROUP) name else null
            )
        }
    }
}