/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.wivewa.android.database.model.Address
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import kotlinx.coroutines.flow.Flow

@Dao
interface AddressDao {
    @Query("DELETE FROM address")
    fun deleteAll()

    @Insert
    fun insert(items: List<Address>)

    @Query("SELECT CASE WHEN entity1_id = :entityId THEN entity2_id WHEN entity2_id = :entityId THEN entity1_id END AS other_entity_id, address.additional, address.street, address.city, address.country, address.zip_code, contact_type.name AS contact_type_name FROM address JOIN contact_type ON (address.contact_type_id = contact_type.id) WHERE entity1_id = :entityId OR entity2_id = :entityId ORDER BY contact_type.name")
    fun getByEntityId(entityId: Long): Flow<List<AddressWithTypeInfo>>
}