/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.util.JsonReader
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "entity_group",
    foreignKeys = [
        ForeignKey(
            entity = Customer::class,
            childColumns = ["owner_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class EntityGroup (
    @PrimaryKey
    val id: Long,
    val name: String,
    val weight: Long,
    @ColumnInfo(name = "owner_id", index = true)
    val ownerId: Long
) {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
        private const val WEIGHT = "weight"
        private const val OWNER_ID = "owner"

        fun fromJson(reader: JsonReader): EntityGroup {
            var id: Long? = null
            var name: String? = null
            var weight: Long? = null
            var ownerId: Long? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextLong()
                    NAME -> name = reader.nextString()
                    WEIGHT -> weight = reader.nextLong()
                    OWNER_ID -> ownerId = reader.nextLong()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return EntityGroup(
                id = id!!,
                name = name!!,
                weight = weight!!,
                ownerId = ownerId!!
            )
        }
    }
}