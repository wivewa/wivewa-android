/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.wivewa.android.database.model.Entity
import de.wivewa.android.database.model.join.ContactListStatus
import de.wivewa.android.database.model.join.EntityWithSalutationAndGroup
import kotlinx.coroutines.flow.Flow

@Dao
interface EntityDao {
    @Query("DELETE FROM entity")
    fun deleteAll()

    @Insert
    fun insert(items: List<Entity>)

    @Query("SELECT * FROM entity ORDER BY LOWER(computed_display_name)")
    fun queryPaged(): PagingSource<Int, Entity>

    @Query("SELECT * FROM entity WHERE INSTR(LOWER(computed_display_name), LOWER(:filterTerm)) ORDER BY LOWER(computed_display_name)")
    fun queryPagedWithFilter(filterTerm: String): PagingSource<Int, Entity>

    @Query("SELECT entity.id, entity.owner_id, entity.type, entity.computed_display_name, entity_group.name AS group_name, entity.person_birthdate, salutation.name AS person_salutation, entity.person_title FROM entity LEFT JOIN entity_group ON (entity.group_id = entity_group.id) LEFT JOIN salutation ON (entity.person_salutation_id = salutation.id) WHERE entity.id = :id")
    fun getByIdFlow(id: Long): Flow<EntityWithSalutationAndGroup?>

    @Query("SELECT * FROM entity WHERE entity.id = :id")
    fun getByIdSync(id: Long): Entity?

    @Query("SELECT * FROM entity WHERE EXISTS (SELECT * FROM contact_info WHERE (entity.id = contact_info.entity1_id OR entity.id = contact_info.entity2_id) AND contact_info.id IN (:contactInfoIds))")
    fun getByContactInfoIdsSync(contactInfoIds: List<Long>): List<Entity>

    @get:Query("SELECT (SELECT CAST(value AS INTEGER) from configuration WHERE key = 'addressbook_last_sync_timestamp') AS last_sync, (SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM customer) AS has_customers, (SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END FROM entity) AS has_contacts")
    val status: Flow<ContactListStatus>
}