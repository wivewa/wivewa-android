/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import androidx.room.*
import androidx.room.Entity

@Entity(
    tableName = "logged_phone_call",
    indices = [
        Index(
            name = "phone_call_sync_index",
            value = ["synced"]
        ),
        Index(
            name = "phone_call_status_index",
            value = ["status"]
        )
    ],
    foreignKeys = [
        ForeignKey(
            entity = Customer::class,
            childColumns = ["entity_id"],
            parentColumns = ["id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class LoggedPhoneCall (
    @PrimaryKey
    val id: String,
    val kind: String,
    val from: String,
    val to: String,
    val start: Long,
    val wait: Long,
    val length: Long,
    val status: String,
    val synced: Boolean,
    @ColumnInfo(name = "entity_id", index = true)
    val entityId: Long
) {
    @Transient
    val end = start + wait + length
}

object PhoneCallKindStrings {
    const val CALL = "call"
    const val RING = "ring"
}

object PhoneCallStatusStrings {
    const val DIALING = "dialing"
    const val RINGING = "ringing"
    const val CONNECTED = "connected"
    const val HOLD = "hold"
    const val UNANSWERED = "unanswered"
    const val DISCONNECTED = "disconnected"

    val TYPE_NOT_DONE = setOf(DIALING, RINGING, CONNECTED, HOLD)
    val TYPE_DONE = setOf(UNANSWERED, DISCONNECTED)
}