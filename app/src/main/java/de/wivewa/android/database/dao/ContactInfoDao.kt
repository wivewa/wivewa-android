/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import de.wivewa.android.database.model.ContactInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo
import de.wivewa.android.database.model.parts.MinimalContactInfo
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactInfoDao {
    @Query("DELETE FROM contact_info")
    fun deleteAll()

    @Insert
    fun insert(items: List<ContactInfo>)

    @Query("SELECT CASE WHEN entity1_id = :entityId THEN entity2_id WHEN entity2_id = :entityId THEN entity1_id END AS other_entity_id, contact_info.type, contact_info.info, contact_type.name AS contact_type_name, contact_type.uri_schema AS contact_type_uri_schema FROM contact_info JOIN contact_type ON (contact_info.contact_type_id = contact_type.id) WHERE entity1_id = :entityId OR entity2_id = :entityId AND contact_info.type = 'contact' ORDER BY contact_info.type, contact_type.name")
    fun getByEntityId(entityId: Long): Flow<List<ContactInfoWithTypeInfo>>

    @Query("SELECT contact_info.id, contact_info.info FROM contact_info JOIN contact_type ON (contact_info.contact_type_id = contact_type.id) WHERE contact_type.uri_schema = :schema")
    fun getByUriSchema(schema: String): List<MinimalContactInfo>
}