/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model.join

import androidx.room.ColumnInfo
import java.time.LocalDate

data class EntityWithSalutationAndGroup (
    val id: Long,
    @ColumnInfo(name = "owner_id", index = true)
    val ownerId: Long?,
    val type: String,
    @ColumnInfo(name = "computed_display_name")
    val computedDisplayName: String,
    @ColumnInfo(name = "group_name")
    val groupName: String?,
    @ColumnInfo(name = "person_birthdate")
    val personBirthdate: LocalDate?,
    @ColumnInfo(name = "person_salutation")
    val personSalutation: String?,
    @ColumnInfo(name = "person_title")
    val personTitle: String?,
)