/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.worker

import android.content.Context
import android.content.pm.ApplicationInfo
import android.util.Log
import androidx.work.*
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.network.HttpServerApi
import de.wivewa.android.sync.AddressbookSync
import java.util.concurrent.TimeUnit

class AddressbookSyncWorker (
    context: Context,
    workerParameters: WorkerParameters
): CoroutineWorker(context, workerParameters) {
    companion object {
        private const val LOG_TAG = "AddressbookSyncWorker"
        private const val UNIQUE_WORK_NAME = "SyncAddressbook"

        fun enqueue(workManager: WorkManager) {
            workManager.enqueueUniquePeriodicWork(
                UNIQUE_WORK_NAME,
                ExistingPeriodicWorkPolicy.KEEP,
                PeriodicWorkRequestBuilder<AddressbookSyncWorker>(1, TimeUnit.DAYS)
                    .setConstraints(
                        Constraints.Builder()
                            .setRequiredNetworkType(NetworkType.UNMETERED)
                            .build()
                    )
                    .build()
            )
        }
    }

    private val apiFactory = HttpServerApi.Factory
    private val database = AppDatabase.with(context)

    override suspend fun doWork(): Result {
        val debug = applicationContext.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0

        if (debug) {
            Log.d(LOG_TAG, "doWork() called")
        }

        return try {
            val result = AddressbookSync.execute(apiFactory, database)

            when (result) {
                AddressbookSync.Result.Ok -> Result.success()
                AddressbookSync.Result.ConcurrentSync -> Result.success()
                AddressbookSync.Result.NotProvisioned -> Result.failure()
            }
        } catch (ex: Exception) {
            if (debug) {
                Log.d(LOG_TAG, "got exception", ex)
            }

            Result.retry()
        }
    }
}