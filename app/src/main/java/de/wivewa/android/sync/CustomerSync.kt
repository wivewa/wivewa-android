/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.sync

import de.wivewa.android.database.Database
import de.wivewa.android.database.model.Customer
import de.wivewa.android.database.model.CustomerRole
import de.wivewa.android.network.ServerApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke

object CustomerSync {
    suspend fun execute(api: ServerApi, database: Database) {
        val customers = api.getCustomerInfo()

        Dispatchers.IO.invoke {
            database.runInTransaction {
                database.customerRole.deleteAll()

                for (customer in customers) {
                    database.customer.updateOrInsert(Customer(
                        id = customer.customerId,
                        name = customer.customerName
                    ))

                    database.customerRole.insert(customer.roles.map { role ->
                        CustomerRole(customer.customerId, role)
                    })
                }

                database.customer.deleteOtherIds(customers.map { it.customerId })
            }
        }
    }
}