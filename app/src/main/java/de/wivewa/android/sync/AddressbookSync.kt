/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.sync

import de.wivewa.android.database.Database
import de.wivewa.android.database.model.ConfigurationItem
import de.wivewa.android.database.model.Customer
import de.wivewa.android.network.ServerApiFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import java.util.concurrent.Callable

object AddressbookSync {
    suspend fun execute(apiFactory: ServerApiFactory, database: Database): Result {
        val now = System.currentTimeMillis()

        val (domain, lastSyncTimestamp) = Dispatchers.IO.invoke {
            database.runInTransaction(Callable {
                val domain = database.configuration.getValue(ConfigurationItem.DOMAIN)
                val lastSyncTimestamp = database.configuration.getValue(ConfigurationItem.ADDRESSBOOK_LAST_SYNC_TIMESTAMP)?.toLongOrNull()

                Pair(domain, lastSyncTimestamp)
            })
        }

        if (domain == null) return Result.NotProvisioned

        val api = apiFactory.create(domain)

        val response = api.getAddressbook()

        return Dispatchers.IO.invoke {
            val validSalutationIds = response.salutations.map { it.id }.toSet()

            val entitiesWithoutInvalidSalutationIds = response.entities.map { entity ->
                if (
                    entity.personSalutationId == null ||
                    validSalutationIds.contains(entity.personSalutationId)
                ) entity
                else entity.copy(personSalutationId = null)
            }

            database.runInTransaction (Callable {
                val currentDomain = database.configuration.getValue(ConfigurationItem.DOMAIN)
                val currentLastSyncTimestamp = database.configuration.getValue(ConfigurationItem.ADDRESSBOOK_LAST_SYNC_TIMESTAMP)?.toLongOrNull()

                if (currentDomain != domain) return@Callable Result.ConcurrentSync
                if (lastSyncTimestamp != currentLastSyncTimestamp) return@Callable Result.ConcurrentSync

                for (customer in response.customers) {
                    database.customer.updateOrInsert(
                        Customer(
                            id = customer.id,
                            name = customer.name
                        )
                    )
                }

                database.relatedEntity.deleteAll()
                database.contactInfo.deleteAll()
                database.address.deleteAll()
                database.entity.deleteAll()
                database.entityGroup.deleteAll()
                database.salutation.deleteAll()
                database.contactType.deleteAll()

                database.contactType.insert(response.contactTypes)
                database.salutation.insert(response.salutations)
                database.entityGroup.insert(response.entityGroups)
                database.entity.insert(entitiesWithoutInvalidSalutationIds)
                database.address.insert(response.addresses)
                database.contactInfo.insert(response.contactInfos)
                database.relatedEntity.insert(response.relatedEntity)

                database.configuration.setValue(ConfigurationItem.ADDRESSBOOK_LAST_SYNC_TIMESTAMP, now.toString())

                return@Callable Result.Ok
            })
        }
    }

    sealed class Result {
        object Ok: Result()
        object ConcurrentSync: Result()
        object NotProvisioned: Result()
    }
}