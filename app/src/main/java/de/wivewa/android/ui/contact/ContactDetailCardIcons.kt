/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Chat
import androidx.compose.material.icons.filled.Fax
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Web
import androidx.compose.ui.graphics.vector.ImageVector
import de.wivewa.android.database.model.ContactType
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo

object ContactDetailCardIcons {
    fun forInfo(info: ContactInfoWithTypeInfo): ImageVector? =
        if (info.contactTypeUriSchema == ContactType.UriSchema.TEL) Icons.Filled.Phone
        else if (info.contactTypeUriSchema == ContactType.UriSchema.FAX) Icons.Filled.Fax
        else if (info.contactTypeUriSchema == ContactType.UriSchema.MAILTO) Icons.Filled.Mail
        else if (info.contactTypeUriSchema == ContactType.UriSchema.XMPP) Icons.Filled.Chat
        else if (ContactType.UriSchema.WEB.contains(info.contactTypeUriSchema)) Icons.Filled.Web
        else null
}