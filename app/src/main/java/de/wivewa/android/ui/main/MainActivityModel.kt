/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.main

import android.app.Application
import android.content.ClipData
import android.content.ClipboardManager
import android.content.pm.ApplicationInfo
import android.os.Build
import android.util.Log
import android.webkit.CookieManager
import androidx.compose.material3.SnackbarHostState
import androidx.core.content.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.work.WorkManager
import de.wivewa.android.R
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.database.Database
import de.wivewa.android.database.model.ConfigurationItem
import de.wivewa.android.network.*
import de.wivewa.android.sync.CustomerSync
import de.wivewa.android.ui.ExceptionString
import de.wivewa.android.worker.AddressbookSyncWorker
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.atomic.AtomicBoolean

class MainActivityModel(application: Application): AndroidViewModel(application) {
    companion object {
        private const val LOG_TAG = "MainActivityModel"
        private const val ERROR_DELAY = 500L
    }

    private val debug = application.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
    private val database: Database = AppDatabase.with(application)
    private val apiFactory: ServerApiFactory = HttpServerApi.Factory
    private val clipboardManager = application.getSystemService<ClipboardManager>()!!
    private val workManager = WorkManager.getInstance(getApplication())

    sealed class State {
        object Loading: State()
        data class DomainScreen(val busy: Boolean): State()
        data class LoginScreen(
            val publicKey: String = DeviceAuthentication.getCertificateSha256(),
            val errorMessageToShow: String? = null
        ): State()
        data class SessionScreen(
            val startUrl: String,
            val domain: String
        ): State()
        object SessionEndScreen: State()
        data class LaunchErrorScreen(val errorMessage: String): State()
    }

    private val mutex = Mutex()
    private var state: State = State.Loading
    private val stateLiveInternal = MutableLiveData<State>()
    private val didInit = AtomicBoolean(false)
    private var customerId: Long? = null
    private var route: ClientLoginRoute? = null

    val snackbarHostState = SnackbarHostState()
    val stateLive: LiveData<State> = stateLiveInternal

    private fun setState(newState: State) {
        state = newState
        stateLiveInternal.postValue(newState)
    }

    fun setDomain(domain: String) {
        viewModelScope.launch {
            mutex.withLock {
                if (state is State.DomainScreen) {
                    if (domain.isBlank()) {
                        launch {
                            snackbarHostState.showSnackbar(
                                getApplication<Application>().getString(R.string.domain_error_empty)
                            )
                        }

                        return@launch
                    }

                    setState(State.DomainScreen(busy = true))

                    val errorDelay = launch { delay(ERROR_DELAY) }

                    try {
                        val result = apiFactory.create(domain).performClientLogin(
                            CookieManager.getInstance(),
                            null,
                            null
                        )

                        if (result is ClientLoginResponse.UnknownCert) {
                            Dispatchers.IO.invoke {
                                database.runInTransaction {
                                    if (database.configuration.getValue(ConfigurationItem.DOMAIN) != null)
                                        throw RuntimeException("already configured")

                                    database.configuration.setValue(ConfigurationItem.DOMAIN, domain.lowercase())
                                }
                            }

                            setState(State.LoginScreen())
                        }
                        else if (result is ClientLoginResponse.Other) throw RuntimeException("unexpected server response: ${result.code}")
                        else throw RuntimeException("unexpected server response")
                    } catch (ex: Exception) {
                        if (debug) {
                            Log.d(LOG_TAG, "setDomain() failed", ex)
                        }

                        errorDelay.join()

                        launch {
                            snackbarHostState.showSnackbar(ex.message.toString())
                        }

                        setState(State.DomainScreen(busy = false))
                    }
                }
            }
        }
    }

    fun retryLogin() { tryLogin { it is State.LoginScreen || it is State.LaunchErrorScreen } }
    fun newSession() { tryLogin { it is State.SessionEndScreen } }

    private fun tryLogin(
        isInitialAttempt: Boolean = false,
        condition: (State) -> Boolean
    ) {
        viewModelScope.launch {
            mutex.withLock {
                if (!condition(state)) return@launch

                setState(State.Loading)

                val errorDelay = launch { delay(ERROR_DELAY) }

                val domain = Dispatchers.IO.invoke {
                    database.configuration.getValue(ConfigurationItem.DOMAIN)
                }

                if (domain == null) setState(State.DomainScreen(busy = false))
                else try {
                    val api = apiFactory.create(domain)

                    val result = api.performClientLogin(
                        CookieManager.getInstance(), customerId, route
                    )

                    if (result is ClientLoginResponse.Success) {
                        val hasCustomers = withContext(Dispatchers.IO) {
                            database.customer.count() > 0
                        }

                        if (hasCustomers) {
                            viewModelScope.launch {
                                try {
                                    CustomerSync.execute(api, database)
                                } catch (ex: Exception) {
                                    Log.d(LOG_TAG, "customer sync failed", ex)
                                }
                            }
                        } else {
                            CustomerSync.execute(api, database)
                        }

                        AddressbookSyncWorker.enqueue(workManager)

                        val url = "https://$domain/${(result.initialPath ?: "").removePrefix("/")}"

                        setState(State.SessionScreen(url, domain))
                    } else if (result is ClientLoginResponse.UnknownCert) {
                        if (!isInitialAttempt) {
                            errorDelay.join()

                            showToast(R.string.toast_unknown_cert)
                        }

                        setState(State.LoginScreen())
                    }
                    else if (result is ClientLoginResponse.Other) throw RuntimeException("unexpected server response: ${result.code}")
                    else throw RuntimeException("unexpected server response")
                } catch (ex: Exception) {
                    if (debug) {
                        Log.d(LOG_TAG, "tryLogin() failed", ex)
                    }

                    errorDelay.join()

                    setState(State.LaunchErrorScreen(ExceptionString.convert(ex)))
                }
            }
        }
    }

    fun copyPublicKeyToClipboard() {
        viewModelScope.launch {
            mutex.withLock {
                val context = getApplication<Application>()
                val state = state

                if (state is State.LoginScreen) {
                    if (debug) {
                        Log.d(LOG_TAG, "public key: ${state.publicKey}")
                    }

                    clipboardManager.setPrimaryClip(
                        ClipData.newPlainText(
                            context.getString(R.string.label_public_key),
                            state.publicKey
                        )
                    )

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU)
                        showToast(R.string.toast_copied_to_clipboard)
                }
            }
        }
    }

    fun endSession() {
        viewModelScope.launch {
            mutex.withLock {
                if (state is State.SessionScreen) {
                    setState(State.SessionEndScreen)
                }
            }
        }
    }

    fun reportUnsupportedLink() {
        showToast(R.string.toast_unsupported_link)
    }

    private fun showToast(message: Int) = showToast(getApplication<Application>().getString(message))

    private fun showToast(message: String) = viewModelScope.launch(Dispatchers.Main) {
        snackbarHostState.showSnackbar(message)
    }

    fun init(customerId: Long?, route: ClientLoginRoute?) {
        if (didInit.compareAndSet(false, true)) {
            this.customerId = customerId
            this.route = route

            tryLogin(isInitialAttempt = true) { it is State.Loading }
        }
    }
}