/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckBox
import androidx.compose.material.icons.filled.CheckBoxOutlineBlank
import androidx.compose.material.icons.filled.Error
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import de.wivewa.android.R
import de.wivewa.android.ui.Common

@Composable
fun CallUploadCard(state: PhoneModel.CallUploadState?) {
    SettingsCard(stringResource(R.string.call_log_title)) {
        Text(stringResource(R.string.call_log_description))

        if (state == null) Common.LoadingRow()
        else {
            for (item in state.accounts) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable(
                            enabled = true,
                            role = Role.Button,
                            onClick = {
                                if (state.edit is PhoneModel.CallUploadState.EditAccount.None)
                                    state.edit.start(item)
                            }
                        )
                        .padding(8.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Column (
                        modifier = Modifier.weight(1.0f)
                    ) {
                        val label = item.label
                        val labelWithCustomer =
                            if (item.customer == null) label
                            else stringResource(
                                R.string.call_log_account_with_customer,
                                label,
                                item.customer.name
                            )

                        Text(labelWithCustomer, style = MaterialTheme.typography.bodyLarge)

                        val errorMessage = when (item.issue) {
                            PhoneModel.CallUploadState.Account.Issue.LostUploadPermission -> stringResource(
                                R.string.call_log_account_error_upload_permission_missing
                            )
                            PhoneModel.CallUploadState.Account.Issue.LostPhoneAccount -> stringResource(
                                R.string.call_log_account_error_phone_account_missing
                            )
                            null -> null
                        }

                        if (errorMessage != null) {
                            Text(
                                errorMessage,
                                style = MaterialTheme.typography.bodySmall,
                                color = MaterialTheme.colorScheme.error
                            )
                        }
                    }

                    val (icon, text) =
                        if (item.issue != null) Pair(Icons.Default.Error, stringResource(R.string.call_log_account_state_error))
                        else if (item.customer != null) Pair(Icons.Default.CheckBox, stringResource(R.string.call_log_account_state_enabled))
                        else Pair(Icons.Default.CheckBoxOutlineBlank, stringResource(R.string.call_log_account_state_disabled))

                    Icon(
                        imageVector = icon,
                        contentDescription = text
                    )
                }
            }

            when (state.issue) {
                PhoneModel.CallUploadState.Issue.ApiNotReady -> Text(
                    stringResource(R.string.phone_api_dependency_not_ready),
                    color = if (state.accounts.any { it.customer != null }) MaterialTheme.colorScheme.error
                    else Color.Unspecified
                )
                is PhoneModel.CallUploadState.Issue.MissingOsPermission -> {
                    Text(stringResource(R.string.call_log_missing_os_permission))

                    Button(
                        onClick = state.issue.requestPermission,
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text(stringResource(R.string.generic_request_permission))
                    }
                }
                is PhoneModel.CallUploadState.Issue.LastSyncFailed -> {
                    var showErrorDetails by rememberSaveable { mutableStateOf(false) }

                    Text(
                        stringResource(R.string.call_log_last_sync_failed),
                        color = MaterialTheme.colorScheme.error
                    )

                    Button(
                        onClick = { showErrorDetails = true },
                        modifier = Modifier.align(Alignment.End)
                    ) {
                        Text(stringResource(R.string.button_error_details))
                    }

                    if (showErrorDetails) {
                        Common.ErrorDetailsDialog(
                            message = state.issue.exception,
                            close = { showErrorDetails = false }
                        )
                    }
                }
                null -> {/* nothing to do */}
            }

            if (state.edit is PhoneModel.CallUploadState.EditAccount.Some) {
                val items = state.edit.customers.map { customer ->
                    Triple(
                        customer.id == state.edit.account.customer?.id,
                        customer.name
                    ) { state.edit.update(customer) }
                } + Triple(
                    state.edit.account.customer == null,
                    stringResource(R.string.call_log_account_no_customer)
                ) { state.edit.update(null) }

                Dialog(
                    onDismissRequest = state.edit.cancel
                ) {
                    Surface(
                        shape = AlertDialogDefaults.shape
                    ) {
                        LazyColumn (
                            modifier = Modifier.padding(8.dp)
                        ) {
                            items(items) { (checked, text, click) ->
                                Button(
                                    onClick = click,
                                    modifier = Modifier.fillMaxWidth(),
                                    enabled = !checked
                                ) {
                                    Text(text)
                                }
                            }

                            item {
                                if (state.edit.customers.isEmpty()) {
                                    Text(
                                        stringResource(R.string.call_log_account_no_customer_dialog),
                                        modifier = Modifier.padding(8.dp)
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}