/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.about

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.wivewa.android.R

@Composable
fun AboutColumn(info: AppInfo) {
    val context = LocalContext.current

    fun launch(uri: String) {
        try {
            context.startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(context, R.string.about_error_toast, Toast.LENGTH_SHORT).show()
        }
    }

    Column (
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier.padding(0.dp, 8.dp)
    ) {
        Column {
            Text(
                info.appTitle,
                style = MaterialTheme.typography.headlineMedium,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(8.dp, 0.dp)
                    .fillMaxWidth()
            )

            Text(
                "Version ${info.appVersion}",
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(8.dp, 0.dp)
                    .fillMaxWidth()
            )
        }

        Column {
            var showDetails by rememberSaveable { mutableStateOf(false) }

            if (!showDetails) {
                if (info.license != null) Text(
                    stringResource(R.string.about_license_text, info.license.title),
                    modifier = Modifier.padding(8.dp, 0.dp)
                )

                TextButton(onClick = { showDetails = true }) {
                    Text(stringResource(R.string.about_show_details))
                }
            } else {
                Text(
                    info.copyrightText,
                    modifier = Modifier.padding(8.dp, 0.dp)
                )

                if (info.license != null) TextButton(onClick = { launch(info.license.url) }) {
                    Text(stringResource(R.string.about_open_license))
                }
            }
        }

        if (info.libraries.isNotEmpty()) {
            Text(
                stringResource(R.string.about_contained_software),
                style = MaterialTheme.typography.headlineSmall,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(8.dp, 0.dp)
                    .fillMaxWidth()
            )

            for (library in info.libraries) {
                Column {
                    Column(
                        modifier = Modifier.padding(8.dp, 0.dp)
                    ) {
                        Text(
                            library.title,
                            style = MaterialTheme.typography.bodyLarge
                        )

                        Text(
                            library.license.title,
                            style = MaterialTheme.typography.bodySmall
                        )
                    }

                    Row (
                        modifier = Modifier.requiredWidth(IntrinsicSize.Max)
                    ) {
                        if (library.libraryUrl != null)
                            TextButton(onClick = { launch (library.libraryUrl) }) {
                                Text(stringResource(R.string.about_open_website))
                            }

                        TextButton(onClick = { launch (library.licenseUrl ?: library.license.url) }) {
                            Text(stringResource(R.string.about_open_license))
                        }
                    }
                }
            }
        }
    }
}