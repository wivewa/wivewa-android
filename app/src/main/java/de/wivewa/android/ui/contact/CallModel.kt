/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import androidx.activity.result.ActivityResultCallback
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch

class CallModel(application: Application): AndroidViewModel(application) {
    sealed class ActivityCommand {
        object RequestCallPermission: ActivityCommand()
        data class StartCall(val number: String, val action: CallAction): ActivityCommand()
    }

    enum class CallAction {
        Call, Dial
    }

    private val activityCommandChannelInternal = Channel<ActivityCommand>()
    private val permissionCallbackChannel = Channel<Boolean>()

    val activityCommand: ReceiveChannel<ActivityCommand> = activityCommandChannelInternal

    val permissionCallback: ActivityResultCallback<Boolean> = ActivityResultCallback<Boolean> {
        // this is only delivered if someone is listening for it
        permissionCallbackChannel.trySend(it)
    }

    fun startCall(number: String) {
        viewModelScope.launch {
            if (ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                activityCommandChannelInternal.send(ActivityCommand.StartCall(number, CallAction.Call))
            } else {
                launch {
                    val didGrantPermission = permissionCallbackChannel.receive()

                    activityCommandChannelInternal.send(ActivityCommand.StartCall(
                        number,
                        if (didGrantPermission) CallAction.Call else CallAction.Dial
                    ))
                }

                activityCommandChannelInternal.send(ActivityCommand.RequestCallPermission)
            }
        }
    }
}