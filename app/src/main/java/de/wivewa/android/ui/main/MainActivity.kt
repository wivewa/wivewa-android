/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.main

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CopyAll
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.wivewa.android.R
import de.wivewa.android.extensions.getParcelableExtraCompat
import de.wivewa.android.network.ClientLoginRoute
import de.wivewa.android.ui.Common
import de.wivewa.android.ui.about.AboutActivity
import de.wivewa.android.ui.settings.SettingsActivity
import de.wivewa.android.ui.theme.AppTheme
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull


class MainActivity : ComponentActivity() {
    companion object {
        private const val CUSTOMER_ID = "customerId"
        private const val ROUTE = "route"

        fun buildIntent(context: Context, customerId: Long?, route: ClientLoginRoute?) = Intent(context, MainActivity::class.java).also { intent ->
            customerId?.let { intent.putExtra(CUSTOMER_ID, it) }
            route?.let { intent.putExtra(ROUTE, it) }
        }
    }

    private val model by viewModels<MainActivityModel>()
    private val webViewModel by viewModels<WebViewModel>()

    private val pickFiles = registerForActivityResult(ActivityResultContracts.OpenMultipleDocuments()) {
        webViewModel.handleFilePickerResult(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in webViewModel.command) when (command) {
                    is WebViewModel.ActivityCommand.OpenFilePicker -> try {
                        pickFiles.launch(command.mimeTypes)
                    } catch (ex: ActivityNotFoundException) {
                        model.reportUnsupportedLink()
                        webViewModel.handleFilePickerResult(emptyList())
                    }
                }
            }
        }

        lifecycleScope.launch {
            val content = findViewById<View>(android.R.id.content)
            val blockDrawing = OnPreDrawListener { false }

            content.viewTreeObserver.addOnPreDrawListener(blockDrawing)

            withTimeoutOrNull(3000) {
                model.stateLive.asFlow().firstOrNull { it != MainActivityModel.State.Loading }
            }

            content.viewTreeObserver.removeOnPreDrawListener(blockDrawing)
        }

        val customerId =
            if (intent.hasExtra(CUSTOMER_ID)) intent.getLongExtra(CUSTOMER_ID, 0)
            else null

        val route = intent.getParcelableExtraCompat<ClientLoginRoute>(ROUTE)

        model.init(customerId, route)

        enableEdgeToEdge()

        setContent {
            AppTheme {
                MainScreen(
                    model,
                    webViewModel,
                    finish = { finish() } ,
                    launchSettings = {
                        startActivity(
                            Intent(this, SettingsActivity::class.java)
                        )
                    },
                    launchAbout = { AboutActivity.launch(this) },
                    openExternalUrl = { url ->
                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, url))
                        } catch (ex: ActivityNotFoundException) {
                            model.reportUnsupportedLink()
                        }
                    }
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(
    model: MainActivityModel,
    webViewModel: WebViewModel,
    finish: () -> Unit,
    launchSettings: () -> Unit,
    launchAbout: () -> Unit,
    openExternalUrl: (Uri) -> Unit
) {
    val stateLive by model.stateLive.observeAsState(MainActivityModel.State.Loading)

    val minimalUi = stateLive is MainActivityModel.State.SessionScreen

    Scaffold(
        topBar = {
            if (!minimalUi) {
                TopAppBar(
                    colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                    title = {
                        Text(stringResource(R.string.app_name))
                    },
                    actions = {
                        IconButton(onClick = launchAbout) {
                            Icon(Icons.Outlined.Info, stringResource(R.string.about_title))
                        }
                    }
                )
            }
        },
        snackbarHost = { SnackbarHost(model.snackbarHostState) }
    ) { contentPadding ->
        Box(
            modifier = Modifier.padding(contentPadding)
        ) {
            when (val state = stateLive) {
                MainActivityModel.State.Loading -> Common.LoadingScreen()
                is MainActivityModel.State.DomainScreen -> DomainScreen(
                    state,
                    setDomain = model::setDomain
                )
                is MainActivityModel.State.LoginScreen -> LoginScreen(
                    state,
                    copyPublicKeyToClipboard = model::copyPublicKeyToClipboard,
                    retryLogin = model::retryLogin
                )
                is MainActivityModel.State.SessionScreen -> SessionScreen(
                    state,
                    model = webViewModel,
                    endSession = model::endSession,
                    launchSettings = launchSettings,
                    openExternalUrl = openExternalUrl
                )
                MainActivityModel.State.SessionEndScreen -> SessionEndScreen(
                    newSession = model::newSession,
                    finish = finish
                )
                is MainActivityModel.State.LaunchErrorScreen -> LaunchErrorScreen(
                    state,
                    retryLogin = model::retryLogin
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DomainScreen(
    state: MainActivityModel.State.DomainScreen,
    setDomain: (String) -> Unit
) {
    var domain by rememberSaveable { mutableStateOf("") }

    val go = { setDomain(domain) }

    Column (
        modifier = Modifier.padding(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(stringResource(R.string.domain_title), style = MaterialTheme.typography.headlineLarge)
        Text(stringResource(R.string.domain_description))

        TextField(
            value = domain,
            onValueChange = { domain = it },
            label = { Text(stringResource(R.string.domain_hint)) },
            modifier = Modifier.fillMaxWidth(),
            enabled = !state.busy,
            singleLine = true,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Go),
            keyboardActions = KeyboardActions(onGo = { go() })
        )

        Button(
            onClick = go,
            modifier = Modifier.align(Alignment.End),
            enabled = !state.busy
        ) {
            Text(stringResource(R.string.generic_button_next))
        }
    }
}

@Composable
fun LoginScreen(
    state: MainActivityModel.State.LoginScreen,
    copyPublicKeyToClipboard: () -> Unit,
    retryLogin: () -> Unit
) {
    Column (
        modifier = Modifier.padding(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        Text(stringResource(R.string.public_key_title), style = MaterialTheme.typography.headlineLarge)
        Text(stringResource(R.string.public_key_text))

        Card (
            modifier = Modifier.fillMaxWidth()
        ) {
            Row (
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    state.publicKey,
                    modifier = Modifier
                        .padding(8.dp)
                        .weight(1F)
                )

                IconButton(onClick = { copyPublicKeyToClipboard() }) {
                    Icon(
                        imageVector = Icons.Filled.CopyAll,
                        contentDescription = stringResource(R.string.generic_copy_to_clipboard)
                    )
                }
            }
        }

        Button(
            onClick = { retryLogin() },
            modifier = Modifier.align(Alignment.End)
        ) {
            Text(stringResource(R.string.generic_button_next))
        }
    }
}

internal enum class WebviewErrorState {
    Working,
    ErrorScreen,
    ErrorRetry
}

@Composable
fun SessionEndScreen(
    newSession: () -> Unit,
    finish: () -> Unit
) {
    Box (
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(stringResource(R.string.session_end_text))

            OutlinedButton(
                onClick = newSession
            ) {
                Text(stringResource(R.string.session_end_restart))
            }

            OutlinedButton(
                onClick = finish
            ) {
                Text(stringResource(R.string.session_end_exit))
            }
        }
    }
}

@Composable
fun LaunchErrorScreen(
    state: MainActivityModel.State.LaunchErrorScreen,
    retryLogin: () -> Unit
) {
    var showDialog by rememberSaveable { mutableStateOf(false) }

    Box (
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            Text(stringResource(R.string.connect_error_text))

            Button(
                onClick = retryLogin
            ) {
                Text(stringResource(R.string.connect_error_action))
            }

            TextButton(
                onClick = { showDialog = true }
            ) {
                Text(stringResource(R.string.button_error_details))
            }

            if (showDialog) {
                Common.ErrorDetailsDialog(
                    message = state.errorMessage,
                    close = { showDialog = false }
                )
            }
        }
    }
}