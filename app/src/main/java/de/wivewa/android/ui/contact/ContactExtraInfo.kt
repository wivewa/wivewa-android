/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder
import de.wivewa.android.database.model.ContactType
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo
import java.util.*

object ContactExtraInfo {
    private val util = PhoneNumberUtil.getInstance()
    private val geocoder = PhoneNumberOfflineGeocoder.getInstance()
    private val carrier = PhoneNumberToCarrierMapper.getInstance()

    fun forContactInfo(info: ContactInfoWithTypeInfo): Pair<String, String?> =
        if (info.contactTypeUriSchema == ContactType.UriSchema.TEL) forContactInfo(info.info)
        else Pair(info.info, null)

    fun forContactInfo(callerNumber: String): Pair<String, String?> {
        try {
            val formatter = util.getAsYouTypeFormatter(Locale.getDefault().country)
            val formattedNumber = callerNumber.map { formatter.inputDigit(it) }.lastOrNull() ?: callerNumber

            val number = util.parse(callerNumber, Locale.getDefault().country)

            val location = geocoder.getDescriptionForNumber(number, Locale.getDefault())
            val carrier = carrier.getNameForNumber(number, Locale.getDefault())

            val infoString = listOf(location, carrier).filterNot { it.isBlank() }

            if (infoString.isNotEmpty()) return Pair(
                formattedNumber,
                infoString.joinToString(separator = ", ")
            )
        } catch (ex: NumberParseException) {/* ignore */}

        return Pair(callerNumber, null)
    }

    fun forAddress(address: AddressWithTypeInfo): Pair<String, String?> = Pair(address.printable, null)
}