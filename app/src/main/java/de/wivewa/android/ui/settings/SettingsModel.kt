/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import android.app.Application
import android.content.pm.ApplicationInfo
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.database.Database
import de.wivewa.android.database.model.ConfigurationItem
import de.wivewa.android.network.HttpServerApi
import de.wivewa.android.network.ServerApiFactory
import de.wivewa.android.sync.CustomerSync
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SettingsModel(application: Application): AndroidViewModel(application) {
    companion object {
        private const val LOG_TAG = "SettingsModel"
    }

    private val debug = application.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
    private val database: Database = AppDatabase.with(application)
    private val apiFactory: ServerApiFactory = HttpServerApi.Factory

    fun init() {
        // this allows ensuring that init is called
    }

    init {
        viewModelScope.launch {
            try {
                val domain = withContext(Dispatchers.IO) {
                    database.configuration.getValue(ConfigurationItem.DOMAIN)
                }

                if (domain != null) {
                    val api = apiFactory.create(domain)

                    CustomerSync.execute(api, database)
                }
            } catch (ex: Exception) {
                if (debug) {
                    Log.d(LOG_TAG, "customer sync failed", ex)
                }
            }
        }
    }
}