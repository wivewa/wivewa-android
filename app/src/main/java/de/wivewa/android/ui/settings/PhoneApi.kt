/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import de.wivewa.android.R
import de.wivewa.android.ui.Common

@Composable
fun PhoneApiStatusCard(state: PhoneModel.ApiState?) {
    SettingsCard(title = stringResource(R.string.phone_api_title)) {
        Text(stringResource(R.string.phone_api_description))

        when (state) {
            PhoneModel.ApiState.UnsupportedDialer -> {
                Text(stringResource(R.string.phone_api_unsupported_dialer))
            }
            PhoneModel.ApiState.ConnectionError -> {
                Text(stringResource(R.string.phone_api_communication_error))
            }
            is PhoneModel.ApiState.MissingPermission -> {
                Text(stringResource(R.string.phone_api_missing_permission))

                Button(
                    onClick = state.requestPermission,
                    modifier = Modifier.align(Alignment.End)
                ) {
                    Text(stringResource(R.string.generic_request_permission))
                }
            }
            PhoneModel.ApiState.Ready -> {
                Text(stringResource(R.string.phone_api_ready))
            }
            null -> Common.LoadingRow()
        }
    }
}