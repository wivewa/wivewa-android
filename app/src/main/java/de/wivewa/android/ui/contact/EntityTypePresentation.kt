/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.content.Context
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.Groups
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import de.wivewa.android.R
import de.wivewa.android.database.model.Entity

data class EntityTypePresentation (
    val label: String,
    val icon: ImageVector
) {
    companion object {
        fun fromType(type: String, context: Context): EntityTypePresentation = when (type) {
            Entity.TYPE_PERSON -> EntityTypePresentation(
                context.getString(R.string.contacts_type_person),
                Icons.Filled.Person
            )
            Entity.TYPE_ORGANIZATION-> EntityTypePresentation(
                context.getString(R.string.contacts_type_organization),
                Icons.Filled.Home
            )
            Entity.TYPE_USERGROUP -> EntityTypePresentation(
                context.getString(R.string.contacts_type_usergroup),
                Icons.Filled.Groups
            )
            else -> EntityTypePresentation(
                context.getString(R.string.contacts_type_unknown),
                Icons.Filled.Error
            )
        }
    }
}