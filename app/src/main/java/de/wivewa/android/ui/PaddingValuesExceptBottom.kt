/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

data class PaddingValuesExceptBottom(val base: PaddingValues): PaddingValues {
    override fun calculateLeftPadding(layoutDirection: LayoutDirection) =
        base.calculateLeftPadding(layoutDirection)

    override fun calculateTopPadding() =
        base.calculateTopPadding()

    override fun calculateRightPadding(layoutDirection: LayoutDirection) =
        base.calculateRightPadding(layoutDirection)

    override fun calculateBottomPadding() =
        0.dp
}