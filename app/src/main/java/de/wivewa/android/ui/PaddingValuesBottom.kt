/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp

data class PaddingValuesBottom (val base: PaddingValues): PaddingValues {
    override fun calculateLeftPadding(layoutDirection: LayoutDirection) =
        0.dp

    override fun calculateTopPadding() =
        0.dp

    override fun calculateRightPadding(layoutDirection: LayoutDirection) =
        0.dp

    override fun calculateBottomPadding() =
        base.calculateBottomPadding()
}