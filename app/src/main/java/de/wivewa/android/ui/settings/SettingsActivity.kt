/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.wivewa.android.R
import de.wivewa.android.ui.about.AboutActivity
import de.wivewa.android.ui.theme.AppTheme
import kotlinx.coroutines.launch

class SettingsActivity : ComponentActivity() {
    private val model by viewModels<SettingsModel>()
    private val phoneModel by viewModels<PhoneModel>()

    private val phoneStatePermission = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
        phoneModel.phoneStateAndNumbersCallback.onActivityResult(it)
    }

    private val requestDialerIntegrationPermission = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) phoneModel.retryGetSystemIntegrationState()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.init()

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in phoneModel.activityCommand) {
                    when (command) {
                        PhoneModel.ActivityCommand.RequestPhoneStateAndPhoneNumbersPermission -> {
                            phoneStatePermission.launch(
                                arrayOf(
                                    Manifest.permission.READ_PHONE_STATE,
                                    Manifest.permission.READ_PHONE_NUMBERS
                                )
                            )
                        }
                        is PhoneModel.ActivityCommand.RequestDialerIntegrationPermission -> try {
                            requestDialerIntegrationPermission.launch(command.intent)
                        } catch (ex: ActivityNotFoundException) {
                            Toast.makeText(this@SettingsActivity, R.string.generic_error, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        enableEdgeToEdge()

        setContent {
            AppTheme {
                SettingsScreen(
                    phoneModel = phoneModel,
                    finish = { finish() },
                    launchAbout = { AboutActivity.launch(this) }
                )
            }
        }
    }

    override fun onStart() {
        super.onStart()

        phoneModel.retryGetSystemIntegrationState()
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SettingsScreen(
    phoneModel: PhoneModel,
    finish: () -> Unit,
    launchAbout: () -> Unit
) {
    val phoneApiState by phoneModel.systemIntegrationState.collectAsState(null)
    val callerInfoState by phoneModel.callerInfoState.collectAsState(null)
    val callUploadState by phoneModel.callUploadState.collectAsState(null)

    Scaffold(
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                title = {
                    Text(stringResource(R.string.app_name))
                },
                navigationIcon = {
                    IconButton(
                        onClick = { finish() }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = stringResource(R.string.topbar_back)
                        )
                    }
                },
                actions = {
                    IconButton(onClick = launchAbout) {
                        Icon(Icons.Outlined.Info, stringResource(R.string.about_title))
                    }
                }
            )
        },
        snackbarHost = { SnackbarHost(phoneModel.snackbarHostState) }
    ) { contentPadding ->
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(contentPadding)
                .padding(8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            PhoneApiStatusCard(phoneApiState)
            CallerInfoCard(callerInfoState)
            CallUploadCard(callUploadState)
        }
    }
}