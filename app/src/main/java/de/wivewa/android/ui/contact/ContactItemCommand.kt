/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.net.Uri
import de.wivewa.android.database.model.join.EntityWithSalutationAndGroup

sealed class ContactItemCommand {
    class CopyToClipboard(val type: String, val data: String): ContactItemCommand()
    class View(val uri: Uri): ContactItemCommand()
    class SendTo(val uri: Uri): ContactItemCommand()
    class Call(val number: String): ContactItemCommand()
    class LaunchExternal(val contact: EntityWithSalutationAndGroup): ContactItemCommand()
}