/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2024 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.content.Context
import android.net.Uri
import de.wivewa.android.R
import de.wivewa.android.database.model.ContactType
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo

data class ContactItemScreenAction (
    val label: String,
    val handler: () -> Unit
) {
    companion object {
        fun forInfo(
            info: ContactInfoWithTypeInfo,
            executeCommand: (ContactItemCommand) -> Unit,
            context: Context
        ): ContactItemScreenAction? {
            return if (info.contactTypeUriSchema == ContactType.UriSchema.TEL) ContactItemScreenAction(
                label = context.getString(R.string.contact_action_phone),
                handler = {
                    executeCommand(ContactItemCommand.Call(info.info))
                }
            )
            else if (info.contactTypeUriSchema == ContactType.UriSchema.MAILTO) ContactItemScreenAction(
                label = context.getString(R.string.contact_action_mail),
                handler = {
                    executeCommand(ContactItemCommand.SendTo(Uri.fromParts(ContactType.UriSchema.MAILTO, info.info, null)))
                }
            )
            else if (info.contactTypeUriSchema == ContactType.UriSchema.XMPP) ContactItemScreenAction(
                label = context.getString(R.string.contact_action_chat),
                handler = {
                    executeCommand(ContactItemCommand.View(Uri.fromParts(ContactType.UriSchema.XMPP, info.info, null)))
                }
            )
            else if (ContactType.UriSchema.WEB.contains(info.contactTypeUriSchema)) {
                val uri = Uri.parse(
                    info.info.let {
                        if (it.contains(":")) it
                        else "https://$it"
                    }
                )

                if (ContactType.UriSchema.WEB.contains(uri.scheme)) {
                    ContactItemScreenAction(
                        label = context.getString(R.string.contact_action_web),
                        handler = {
                            executeCommand(ContactItemCommand.View(uri))
                        }
                    )
                } else null
            } else null
        }

        fun forAddress(
            info: AddressWithTypeInfo,
            executeCommand: (ContactItemCommand) -> Unit,
            context: Context
        ) = ContactItemScreenAction(
            label = context.getString(R.string.contact_action_map),
            handler = {
                executeCommand(
                    ContactItemCommand.View(
                        Uri.fromParts("geo", "0,0", null)
                            .buildUpon()
                            .appendQueryParameter(
                                "q",
                                clean(info.printable)
                            )
                            .build()
                    )
                )
            }
        )

        private fun clean(input: String) =
            input.replace('\n', ' ')
                .replace("(", "")
                .replace(")", "")
                .trim()
    }
}