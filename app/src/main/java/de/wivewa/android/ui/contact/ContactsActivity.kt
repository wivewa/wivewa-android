/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.*
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.core.content.getSystemService
import androidx.core.view.WindowCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.wivewa.android.database.model.ContactType
import de.wivewa.android.network.ClientLoginRoute
import de.wivewa.android.ui.Common
import de.wivewa.android.ui.about.AboutActivity
import de.wivewa.android.ui.main.MainActivity
import de.wivewa.android.ui.theme.TransitionAnimation
import de.wivewa.android.ui.theme.AppTheme
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch

class ContactsActivity : ComponentActivity() {
    companion object {
        private const val URI_SCHEMA = "contact"

        fun createIntentForContact(context: Context, id: Long) = Intent(context, ContactsActivity::class.java)
            .setData(Uri.fromParts(URI_SCHEMA, id.toString(), null))
    }

    private val model by viewModels<ContactsModel>()
    private val callModel by viewModels<CallModel>()

    private val requestCallPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        callModel.permissionCallback.onActivityResult(it)
    }

    private val contentIsReady = Channel<Unit>(Channel.CONFLATED)

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.data?.scheme == URI_SCHEMA) {
            intent.data?.schemeSpecificPart?.toLongOrNull()?.let { id ->
                model.openInitialContact(id)
            }
        }

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in callModel.activityCommand) {
                    when (command) {
                        CallModel.ActivityCommand.RequestCallPermission -> requestCallPermission.launch(Manifest.permission.CALL_PHONE)
                        is CallModel.ActivityCommand.StartCall -> launchIntent(Intent(
                            when (command.action) {
                                CallModel.CallAction.Call -> Intent.ACTION_CALL
                                CallModel.CallAction.Dial -> Intent.ACTION_DIAL
                            },
                            Uri.fromParts(ContactType.UriSchema.TEL, command.number, null)
                        ))
                    }
                }
            }
        }

        WindowCompat.setDecorFitsSystemWindows(window, false)

        lifecycleScope.launch {
            val content = findViewById<View>(android.R.id.content)
            val blockDrawing = OnPreDrawListener { false }

            content.viewTreeObserver.addOnPreDrawListener(blockDrawing)
            contentIsReady.receive()
            content.viewTreeObserver.removeOnPreDrawListener(blockDrawing)
        }

        lifecycleScope.launch {
            model.screen.firstOrNull { it !is ContactsModel.Screen.ContactList }
            contentIsReady.trySend(Unit)
        }

        enableEdgeToEdge()

        setContent {
            val screen by model.screen.collectAsState(null)
            val errorLive by model.errorState.collectAsState()

            AppTheme {
                updateTransition(targetState = screen, label = "AnimatedContent").AnimatedContent(
                    transitionSpec = {
                        val from = initialState
                        val to = targetState

                        if (
                            from is ContactsModel.Screen.ContactList &&
                            to is ContactsModel.Screen.ContactItem
                        ) TransitionAnimation.openScreen
                        else if (
                            from is ContactsModel.Screen.ContactItem &&
                            to is ContactsModel.Screen.ContactList
                        ) TransitionAnimation.closeScreen
                        else if (
                            from is ContactsModel.Screen.ContactItem &&
                            to is ContactsModel.Screen.ContactItem
                        ) {
                            if (to.hasPrevious(from)) TransitionAnimation.openScreen
                            else if (from.hasPrevious(to)) TransitionAnimation.closeScreen
                            else TransitionAnimation.none
                        }
                        else TransitionAnimation.none
                    },
                    contentKey = {
                        when (it) {
                            is ContactsModel.Screen.ContactList -> "list"
                            is ContactsModel.Screen.EmptyContactList -> "list"
                            is ContactsModel.Screen.ContactItem -> it.entity.id
                            null -> "null"
                        }
                    },
                    modifier = Modifier.background(Color.Black)
                ) {screen ->
                    when (screen) {
                        is ContactsModel.Screen.EmptyContactList -> EmptyContactList(
                            screen = screen,
                            syncState = model.currentSyncState,
                            syncContactList = model::syncContactList,
                            launchAbout = ::launchAbout
                        )
                        is ContactsModel.Screen.ContactList -> ContactListScreen(
                            screen = screen,
                            syncState = model.currentSyncState,
                            lastSyncWarning = screen.lastSyncWarning,
                            syncContactList = model::syncContactList,
                            launchAbout = ::launchAbout,
                            reportReady = { contentIsReady.trySend(Unit) }
                        )
                        is ContactsModel.Screen.ContactItem -> ContactItemScreen(
                            screen = screen,
                            executeCommand = ::executeContactItemCommand
                        )
                        null -> Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .background(MaterialTheme.colorScheme.background)
                        )
                    }
                }

                val error = errorLive

                if (error is ContactsModel.ErrorDialogState.Show) {
                    Common.ErrorDetailsDialog(
                        message = error.message,
                        close = { error.close() }
                    )
                }
            }
        }
    }

    private fun executeContactItemCommand(command: ContactItemCommand) = when (command) {
        is ContactItemCommand.CopyToClipboard -> copyToClipboard(ClipData.newPlainText(command.type, command.data))
        is ContactItemCommand.View -> launchIntent(Intent(Intent.ACTION_VIEW, command.uri))
        is ContactItemCommand.SendTo -> launchIntent(Intent(Intent.ACTION_SENDTO, command.uri))
        is ContactItemCommand.Call -> callModel.startCall(command.number)
        is ContactItemCommand.LaunchExternal -> startActivity(
            MainActivity.buildIntent(
                this,
                command.contact.ownerId,
                ClientLoginRoute.EntityDetails(command.contact.id, null)
            ).addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_DOCUMENT)
        )
    }

    private fun launchIntent(intent: Intent) {
        try {
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
        } catch (ex: ActivityNotFoundException) {
            model.showMissingAppToast()
        }
    }

    private fun launchAbout() {
        AboutActivity.launch(this)
    }

    private fun copyToClipboard(clipData: ClipData) {
        getSystemService<ClipboardManager>()!!.setPrimaryClip(clipData)
    }
}