/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.main

import android.net.Uri
import android.webkit.JsResult
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.getAndUpdate

class WebViewModel(): ViewModel() {
    sealed class ActivityCommand {
        class OpenFilePicker(val mimeTypes: Array<String>): ActivityCommand()
    }

    sealed class Dialog {
        abstract val message: String
        abstract fun ok()

        class Alert(
            override val message: String,
            private val model: WebViewModel,
            private val result: JsResult
        ): Dialog() {
            override fun ok() {
                if (model.dialogInternal.compareAndSet(this, null)) result.confirm()
            }
        }

        data class Confirm(
            override val message: String,
            private val model: WebViewModel,
            private val result: JsResult
        ): Dialog() {
            override fun ok() {
                if (model.dialogInternal.compareAndSet(this, null)) result.confirm()
            }

            fun cancel() {
                if (model.dialogInternal.compareAndSet(this, null)) result.cancel()
            }
        }
    }

    private val dialogInternal = MutableStateFlow(null as Dialog?)
    private val commandInternal = Channel<ActivityCommand>(1)
    private val fileCallback = MutableStateFlow<ValueCallback<Array<Uri>>?>(null)

    val dialog: Flow<Dialog?> = dialogInternal
    val command: ReceiveChannel<ActivityCommand> = commandInternal

    fun handleFilePickerResult(files: List<Uri>) {
        fileCallback.getAndUpdate { null }?.onReceiveValue(files.toTypedArray())
    }

    val client = object: WebChromeClient() {
        override fun onJsAlert(
            view: WebView,
            url: String,
            message: String,
            result: JsResult
        ): Boolean = dialogInternal.compareAndSet(null, Dialog.Alert(
            message = message,
            model = this@WebViewModel,
            result = result
        ))

        override fun onJsConfirm(
            view: WebView,
            url: String,
            message: String,
            result: JsResult
        ): Boolean  = dialogInternal.compareAndSet(null, Dialog.Confirm(
            message = message,
            model = this@WebViewModel,
            result = result
        ))

        override fun onShowFileChooser(
            webView: WebView,
            filePathCallback: ValueCallback<Array<Uri>>,
            fileChooserParams: FileChooserParams
        ): Boolean = if (fileCallback.compareAndSet(null, filePathCallback)) {
            val types = fileChooserParams.acceptTypes
                .filterNot { it.isBlank() }
                .let {
                    if (it.isEmpty()) listOf("*/*")
                    else it
                }

            commandInternal.trySend(ActivityCommand.OpenFilePicker(types.toTypedArray())).isSuccess
        } else super.onShowFileChooser(webView, filePathCallback, fileChooserParams)
    }
}