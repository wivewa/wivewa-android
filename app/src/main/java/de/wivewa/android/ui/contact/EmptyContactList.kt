/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.wivewa.android.R
import kotlinx.coroutines.flow.StateFlow

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EmptyContactList(
    screen: ContactsModel.Screen.EmptyContactList,
    syncState: StateFlow<ContactsModel.CurrentSyncState>,
    syncContactList: () -> Unit,
    launchAbout: () -> Unit
) {
    val sync by syncState.collectAsState()

    Scaffold (
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                title = {
                    Text(stringResource(R.string.contacts_activity_title))
                },
                actions = {
                    IconButton(onClick = launchAbout) {
                        Icon(Icons.Outlined.Info, stringResource(R.string.about_title))
                    }
                }
            )
        },
        snackbarHost = { SnackbarHost(screen.snackbarHostState) }
    ) { contentPadding ->
        Column (
            modifier = Modifier
                .padding(contentPadding)
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically)
        ) {
            Text(
                stringResource(R.string.contacts_empty_title),
                style = MaterialTheme.typography.headlineMedium
            )

            Text(
                stringResource(
                    if (!screen.hasCustomers)
                        R.string.contacts_empty_text_login
                    else if (!screen.hadSync)
                        R.string.contacts_empty_text_never_synced
                    else
                        R.string.contacts_empty_text_permission
                ),
                textAlign = TextAlign.Center
            )

            if (screen.hasCustomers) {
                Button(
                    onClick = syncContactList,
                    enabled = sync is ContactsModel.CurrentSyncState.Idle
                ) {
                    Text(stringResource(R.string.contacts_empty_action_sync_now))
                }
            }
        }
    }
}