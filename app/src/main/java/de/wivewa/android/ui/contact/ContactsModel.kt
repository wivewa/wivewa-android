/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.app.Application
import android.content.pm.ApplicationInfo
import android.text.format.DateUtils
import android.util.Log
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import de.wivewa.android.R
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.database.model.Entity
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo
import de.wivewa.android.database.model.join.EntityWithSalutationAndGroup
import de.wivewa.android.database.model.join.RelatedEntityWithName
import de.wivewa.android.network.HttpServerApi
import de.wivewa.android.sync.AddressbookSync
import de.wivewa.android.ui.ExceptionString
import de.wivewa.android.utils.LocalDateFlow
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import java.time.LocalDate
import java.util.concurrent.atomic.AtomicBoolean

class ContactsModel(application: Application): AndroidViewModel(application) {
    companion object {
        private const val LOG_TAG = "ContactsModel"
        private const val SYNC_WARNING_DELAY = 1000 * 60 * 60 * 24 * 3 // 3 days
    }

    private val debug = application.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
    private val database = AppDatabase.with(application)
    private val apiFactory = HttpServerApi.Factory

    private val localDateFlow = LocalDateFlow.with(application)
        .shareIn(viewModelScope, SharingStarted.WhileSubscribed(stopTimeoutMillis = 1000), 1)
    private val snackbarHostState = SnackbarHostState()

    private val didInitState = AtomicBoolean(false)
    private var state: State = State.ViewContactList()

    private val currentSyncStateInternal: MutableStateFlow<CurrentSyncState> = MutableStateFlow(CurrentSyncState.Idle)
    val currentSyncState: StateFlow<CurrentSyncState> = currentSyncStateInternal

    private val errorStateInternal: MutableStateFlow<ErrorDialogState> = MutableStateFlow(ErrorDialogState.None)
    val errorState: StateFlow<ErrorDialogState> = errorStateInternal

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    val screen = channelFlow {
        didInitState.set(true)

        val syncStateFlow = database.entity.status.shareIn(this, SharingStarted.Eagerly, 1)

        val timestampFlow = flow {
            while (true) {
                emit(System.currentTimeMillis())
                delay(30 * 1000)
            }
        }.shareIn(this, SharingStarted.Eagerly, 1)

        val currentSearchTerm = MutableStateFlow(null as String?)

        val currentContactList = currentSearchTerm
            .map { it?.trim() ?: "" }
            .distinctUntilChanged()
            .debounce(100)
            .flatMapLatest { query ->
                Pager(
                    config = PagingConfig(25),
                    pagingSourceFactory = {
                        if (query == "") database.entity.queryPaged()
                        else database.entity.queryPagedWithFilter(query)
                    }
                ).flow
            }
            .cachedIn(this)

        while (true) {
            when (val currentState = state) {
                is State.ViewContactList -> {
                    val nextState = Channel<State>()

                    val showDetails: (Entity) -> Unit = {
                        nextState.trySend(State.ViewEntity(
                            id = it.id,
                            previous = currentState.copy(
                                searchTerm = currentSearchTerm.value.let {
                                    if (it == "") null else it
                                }
                            )
                        ))
                    }

                    currentSearchTerm.value = currentState.searchTerm

                    val job = launch {
                        combine(syncStateFlow, timestampFlow, currentSearchTerm, localDateFlow) { syncState, timestamp, currentSearchTerm, localDate ->
                            if (syncState.hasContacts) {
                                val lastSyncWarning = if (syncState.lastSync == null)
                                    getApplication<Application>().getString(R.string.last_sync_never)
                                else if (syncState.lastSync + SYNC_WARNING_DELAY < timestamp)
                                    DateUtils.getRelativeTimeSpanString(syncState.lastSync, timestamp, DateUtils.MINUTE_IN_MILLIS).toString()
                                else
                                    null

                                Screen.ContactList(
                                    list = currentContactList,
                                    listState = currentState.listState,
                                    localDate = localDate,
                                    lastSyncWarning = lastSyncWarning,
                                    showDetails = showDetails,
                                    snackbarHostState = snackbarHostState,
                                    searchTerm = currentSearchTerm,
                                    updateSearchTerm = { nextState.trySend(currentState.copy(searchTerm = it)) }
                                )
                            } else {
                                Screen.EmptyContactList(
                                    hasCustomers = syncState.hasCustomers,
                                    hadSync = syncState.lastSync != null,
                                    snackbarHostState = snackbarHostState
                                )
                            }
                        }.distinctUntilChanged().collect {
                            send(it)
                        }
                    }

                    while (true) {
                        val newState = nextState.receive()

                        state = newState

                        if (newState is State.ViewContactList && newState.listState == currentState.listState) {
                            currentSearchTerm.value = newState.searchTerm
                        } else break
                    }

                    job.cancelAndJoin()
                }
                is State.ViewEntity -> {
                    val nextState = Channel<State>()

                    val entityFlow = database.entity.getByIdFlow(currentState.id)
                    val contactInfoFlow = database.contactInfo.getByEntityId(currentState.id)
                    val addressFlow = database.address.getByEntityId(currentState.id)
                    val relatedEntityFlow = database.relatedEntity.getByIdFlow(currentState.id)

                    fun goto(otherEntityId: Long) = currentState.getPrevious(otherEntityId)
                        ?: State.ViewEntity(
                            id = otherEntityId,
                            previous = currentState.copy(otherEntity = null)
                        )

                    val job = launch {
                        combine(
                            combine(entityFlow, contactInfoFlow, addressFlow) { a, b, c -> Triple(a, b, c) },
                            combine(relatedEntityFlow, localDateFlow) { a, b -> Pair(a, b) }
                        ) { a, b -> Pair(a, b) }
                            .collect { (packed1, packed2) ->
                                val (entity, contactInfo, addresses) = packed1
                                val (relatedEntities, localDate) = packed2

                                if (entity == null) {
                                    nextState.trySend(currentState.previous)

                                    return@collect
                                }

                                val contactInfoGrouped = contactInfo.groupBy { it.otherEntityId }
                                val addressesGrouped = addresses.groupBy { it.otherEntityId }

                                fun hasInfoForRelatedEntity(relatedEntity: RelatedEntityWithName) =
                                    contactInfoGrouped.containsKey(relatedEntity.entityId2) ||
                                            addressesGrouped.containsKey(relatedEntity.entityId2)

                                val bottomSheet = if (currentState.otherEntity != null) {
                                    // close the bottom sheet if it would be empty
                                    val relatedEntity = relatedEntities.find { it.entityId2 == currentState.otherEntity.id }

                                    if (relatedEntity == null) {
                                        nextState.trySend(currentState.copy(otherEntity = null))

                                        return@collect
                                    }

                                    when (currentState.otherEntity) {
                                        is State.ViewEntity.OtherEntity.Goto -> {
                                            // handled later after the screen was sent

                                            null
                                        }
                                        is State.ViewEntity.OtherEntity.Show -> {
                                            if (!hasInfoForRelatedEntity(relatedEntity)) {
                                                nextState.trySend(currentState.copy(otherEntity = null))

                                                return@collect
                                            }

                                            Screen.ContactItem.BottomSheet(
                                                entity = relatedEntity,
                                                contactInfo = contactInfoGrouped[relatedEntity.entityId2] ?: emptyList(),
                                                addresses = addressesGrouped[relatedEntity.entityId2] ?: emptyList(),
                                                close = { nextState.trySend(currentState.copy(otherEntity = null)) }
                                            )
                                        }
                                    }
                                } else null

                                send(Screen.ContactItem(
                                    entity = entity,
                                    contactInfo = contactInfoGrouped[null] ?: emptyList(),
                                    addresses = addressesGrouped[null] ?: emptyList(),
                                    relatedEntities = relatedEntities,
                                    localDate = localDate,
                                    goBack = { nextState.trySend(currentState.previous) },
                                    showDetails = {
                                        val show = State.ViewEntity.OtherEntity.Show(it.entityId2)

                                        nextState.trySend(
                                            if (hasInfoForRelatedEntity(it) && currentState.otherEntity != show)
                                                currentState.copy(otherEntity = show)
                                            else if (currentState.otherEntity == show)
                                                currentState.copy(otherEntity = State.ViewEntity.OtherEntity.Goto(it.entityId2))
                                            else
                                                goto(it.entityId2)
                                        )
                                    },
                                    bottomSheet = bottomSheet,
                                    state = currentState,
                                    snackbarHostState = snackbarHostState
                                ))

                                if (currentState.otherEntity is State.ViewEntity.OtherEntity.Goto)
                                    nextState.trySend(goto(currentState.otherEntity.id))
                        }
                    }

                    state = nextState.receive()
                    job.cancelAndJoin()
                }
            }
        }
    }.conflate().shareIn(viewModelScope, SharingStarted.WhileSubscribed(1000), 1)

    fun openInitialContact(id: Long) {
        if (didInitState.getAndSet(true) == false) {
            state = State.ViewEntity(
                id = id,
                previous = State.ViewContactList()
            )
        }
    }

    fun syncContactList() {
        viewModelScope.launch {
            if (currentSyncStateInternal.compareAndSet(CurrentSyncState.Idle, CurrentSyncState.Syncing)) {
                val uxDelay = launch { delay(1000) }

                try {
                    when (
                        AddressbookSync.execute(
                            apiFactory = apiFactory,
                            database = database
                        )
                    ) {
                        AddressbookSync.Result.Ok -> {/* nothing to do */
                        }
                        AddressbookSync.Result.ConcurrentSync -> throw RuntimeException("concurrent sync")
                        AddressbookSync.Result.NotProvisioned -> throw RuntimeException("not provisioned")
                    }

                    uxDelay.join()

                    launch {
                        snackbarHostState.showSnackbar(getApplication<Application>().getString(R.string.contact_sync_success))
                    }
                } catch (ex: Exception) {
                    uxDelay.join()

                    if (debug) {
                        Log.d(LOG_TAG, "syncContactList() failed", ex)
                    }

                    launch {
                        val result = snackbarHostState.showSnackbar(
                            message = getApplication<Application>().getString(R.string.contact_sync_error),
                            actionLabel = getApplication<Application>().getString(R.string.button_error_details),
                            duration = SnackbarDuration.Short
                        )

                        if (result == SnackbarResult.ActionPerformed) {
                            showErrorDialog(ExceptionString.convert(ex))
                        }
                    }
                } finally {
                    currentSyncStateInternal.emit(CurrentSyncState.Idle)
                }
            }
        }
    }

    fun showMissingAppToast() {
        viewModelScope.launch {
            snackbarHostState.showSnackbar(getApplication<Application>().getString(R.string.contact_action_failed_missing_app))
        }
    }

    private fun showErrorDialog(message: String) {
        errorStateInternal.compareAndSet(
            ErrorDialogState.None,
            ErrorDialogState.Show(message, errorStateInternal)
        )
    }

    sealed class State {
        data class ViewContactList(
            val listState: LazyListState = LazyListState(),
            val searchTerm: String? = null
        ): State()
        data class ViewEntity(
            val id: Long,
            val otherEntity: OtherEntity? = null,
            val previous: State
        ): State() {
            sealed class OtherEntity {
                abstract val id: Long

                data class Goto(override val id: Long): OtherEntity()
                data class Show(override val id: Long): OtherEntity()
            }

            fun hasPrevious(other: State): Boolean =
                previous == other ||
                        previous is ViewEntity && other is ViewEntity && previous.id == other.id ||
                        previous is ViewEntity && previous.hasPrevious(other)

            fun getPrevious(entityId: Long): ViewEntity? = if (this.id == entityId) this
            else if (previous is ViewEntity) previous.getPrevious(entityId)
            else null
        }
    }

    sealed class Screen {
        abstract val snackbarHostState: SnackbarHostState

        data class ContactList(
            val list: Flow<PagingData<Entity>>,
            val listState: LazyListState,
            val localDate: LocalDate,
            val searchTerm: String?,
            val updateSearchTerm: (String?) -> Unit,
            val showDetails: (Entity) -> Unit,
            val lastSyncWarning: String?,
            override val snackbarHostState: SnackbarHostState
        ): Screen()

        data class EmptyContactList(
            val hasCustomers: Boolean,
            val hadSync: Boolean,
            override val snackbarHostState: SnackbarHostState
        ): Screen()

        class ContactItem(
            val entity: EntityWithSalutationAndGroup,
            val contactInfo: List<ContactInfoWithTypeInfo>,
            val addresses: List<AddressWithTypeInfo>,
            val relatedEntities: List<RelatedEntityWithName>,
            val bottomSheet: BottomSheet?,
            val localDate: LocalDate,
            val goBack: () -> Unit,
            val showDetails: (RelatedEntityWithName) -> Unit,
            override val snackbarHostState: SnackbarHostState,
            internal val state: State.ViewEntity
        ): Screen() {
            class BottomSheet(
                val entity: RelatedEntityWithName,
                val contactInfo: List<ContactInfoWithTypeInfo>,
                val addresses: List<AddressWithTypeInfo>,
                val close: () -> Unit
            )

            fun hasPrevious(other: ContactItem) = state.hasPrevious(other.state)
        }
    }

    sealed class CurrentSyncState {
        object Idle: CurrentSyncState()
        object Syncing: CurrentSyncState()
    }

    sealed class ErrorDialogState {
        object None: ErrorDialogState()
        class Show(
            val message: String,
            private val errorState: MutableStateFlow<ErrorDialogState>
        ): ErrorDialogState() {
            fun close() {
                errorState.compareAndSet(this, None)
            }
        }
    }
}