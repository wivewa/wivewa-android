/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.theme
import androidx.compose.ui.graphics.Color

val md_theme_light_primary = Color(0xFF695F00)
val md_theme_light_onPrimary = Color(0xFFFFFFFF)
val md_theme_light_primaryContainer = Color(0xFFF9E534)
val md_theme_light_onPrimaryContainer = Color(0xFF201C00)
val md_theme_light_secondary = Color(0xFF006E1C)
val md_theme_light_onSecondary = Color(0xFFFFFFFF)
val md_theme_light_secondaryContainer = Color(0xFF94F990)
val md_theme_light_onSecondaryContainer = Color(0xFF002204)
val md_theme_light_tertiary = Color(0xFF4355B9)
val md_theme_light_onTertiary = Color(0xFFFFFFFF)
val md_theme_light_tertiaryContainer = Color(0xFFDEE0FF)
val md_theme_light_onTertiaryContainer = Color(0xFF00105C)
val md_theme_light_error = Color(0xFFBA1A1A)
val md_theme_light_errorContainer = Color(0xFFFFDAD6)
val md_theme_light_onError = Color(0xFFFFFFFF)
val md_theme_light_onErrorContainer = Color(0xFF410002)
val md_theme_light_background = Color(0xFFFFFBFF)
val md_theme_light_onBackground = Color(0xFF1D1C16)
val md_theme_light_surface = Color(0xFFFFFBFF)
val md_theme_light_onSurface = Color(0xFF1D1C16)
val md_theme_light_surfaceVariant = Color(0xFFE8E2D0)
val md_theme_light_onSurfaceVariant = Color(0xFF4A473A)
val md_theme_light_outline = Color(0xFF7B7768)
val md_theme_light_inverseOnSurface = Color(0xFFF5F0E7)
val md_theme_light_inverseSurface = Color(0xFF32302A)
val md_theme_light_inversePrimary = Color(0xFFDBC90A)
val md_theme_light_shadow = Color(0xFF000000)
val md_theme_light_surfaceTint = Color(0xFF695F00)
val md_theme_light_outlineVariant = Color(0xFFCBC6B5)
val md_theme_light_scrim = Color(0xFF000000)

val md_theme_dark_primary = Color(0xFFDBC90A)
val md_theme_dark_onPrimary = Color(0xFF363100)
val md_theme_dark_primaryContainer = Color(0xFF4F4800)
val md_theme_dark_onPrimaryContainer = Color(0xFFF9E534)
val md_theme_dark_secondary = Color(0xFF78DC77)
val md_theme_dark_onSecondary = Color(0xFF00390A)
val md_theme_dark_secondaryContainer = Color(0xFF005313)
val md_theme_dark_onSecondaryContainer = Color(0xFF94F990)
val md_theme_dark_tertiary = Color(0xFFBAC3FF)
val md_theme_dark_onTertiary = Color(0xFF08218A)
val md_theme_dark_tertiaryContainer = Color(0xFF293CA0)
val md_theme_dark_onTertiaryContainer = Color(0xFFDEE0FF)
val md_theme_dark_error = Color(0xFFFFB4AB)
val md_theme_dark_errorContainer = Color(0xFF93000A)
val md_theme_dark_onError = Color(0xFF690005)
val md_theme_dark_onErrorContainer = Color(0xFFFFDAD6)
val md_theme_dark_background = Color(0xFF1D1C16)
val md_theme_dark_onBackground = Color(0xFFE7E2D9)
val md_theme_dark_surface = Color(0xFF1D1C16)
val md_theme_dark_onSurface = Color(0xFFE7E2D9)
val md_theme_dark_surfaceVariant = Color(0xFF4A473A)
val md_theme_dark_onSurfaceVariant = Color(0xFFCBC6B5)
val md_theme_dark_outline = Color(0xFF959181)
val md_theme_dark_inverseOnSurface = Color(0xFF1D1C16)
val md_theme_dark_inverseSurface = Color(0xFFE7E2D9)
val md_theme_dark_inversePrimary = Color(0xFF695F00)
val md_theme_dark_shadow = Color(0xFF000000)
val md_theme_dark_surfaceTint = Color(0xFFDBC90A)
val md_theme_dark_outlineVariant = Color(0xFF4A473A)
val md_theme_dark_scrim = Color(0xFF000000)


val seed = Color(0xFFFFEB3B)
