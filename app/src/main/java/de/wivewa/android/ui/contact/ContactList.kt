/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cake
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Sync
import androidx.compose.material.icons.outlined.Info
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import de.wivewa.android.R
import de.wivewa.android.database.model.Entity
import de.wivewa.android.extensions.hasSameDateWithinYear
import de.wivewa.android.ui.PaddingValuesBottom
import de.wivewa.android.ui.PaddingValuesExceptBottom
import kotlinx.coroutines.flow.StateFlow
import java.time.LocalDate

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun ContactListScreen(
    screen: ContactsModel.Screen.ContactList,
    syncState: StateFlow<ContactsModel.CurrentSyncState>,
    lastSyncWarning: String?,
    syncContactList: () -> Unit,
    launchAbout: () -> Unit,
    reportReady: () -> Unit
) {
    val list = screen.list.collectAsLazyPagingItems()
    val sync by syncState.collectAsState()

    if (list.itemCount > 0) reportReady()

    Scaffold (
        contentWindowInsets = WindowInsets.safeDrawing,
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                title = {
                    Text(stringResource(R.string.contacts_activity_title))
                },
                actions = {
                    IconButton(onClick = launchAbout) {
                        Icon(Icons.Outlined.Info, stringResource(R.string.about_title))
                    }

                    IconButton(
                        onClick = syncContactList,
                        enabled = sync is ContactsModel.CurrentSyncState.Idle
                    ) {
                        val rotation = when (sync) {
                            ContactsModel.CurrentSyncState.Idle -> 0f
                            ContactsModel.CurrentSyncState.Syncing -> {
                                val infiniteTransition = rememberInfiniteTransition()

                                infiniteTransition.animateFloat(
                                    initialValue = 0f,
                                    targetValue = 360f,
                                    animationSpec = infiniteRepeatable(
                                        animation = tween(
                                            durationMillis = 1000,
                                            easing = LinearEasing
                                        ),
                                        repeatMode = RepeatMode.Restart
                                    )
                                ).value
                            }
                        }

                        Icon(
                            imageVector = Icons.Filled.Sync,
                            contentDescription = stringResource(R.string.contact_sync_action),
                            modifier = Modifier.rotate(rotation)
                        )
                    }

                    AnimatedVisibility(visible = screen.searchTerm == null) {
                        IconButton(onClick = { screen.updateSearchTerm("") }) {
                            Icon(
                                imageVector = Icons.Filled.Search,
                                contentDescription = stringResource(R.string.contact_search_action)
                            )
                        }
                    }
                }
            )
        },
        snackbarHost = { SnackbarHost(screen.snackbarHostState) }
    ) { contentPadding ->
        ContactListScreenContent(
            onItemClicked = screen.showDetails,
            list = list,
            localDate = screen.localDate,
            lastSyncWarning = lastSyncWarning,
            listState = screen.listState,
            searchTerm = screen.searchTerm,
            contentPadding = contentPadding,
            updateSearchTerm = screen.updateSearchTerm
        )
    }
}
@Composable
fun ContactListScreenContent(
    onItemClicked: (Entity) -> Unit,
    list: LazyPagingItems<Entity>,
    localDate: LocalDate,
    lastSyncWarning: String?,
    listState: LazyListState,
    searchTerm: String?,
    contentPadding: PaddingValues,
    updateSearchTerm: (String?) -> Unit,
) {
    val focusRequester = remember { FocusRequester() }
    var lastSearchTerm by remember { mutableStateOf("") }

    Column (
        Modifier.padding(PaddingValuesExceptBottom(contentPadding))
    ) {
        AnimatedVisibility(visible = searchTerm != null) {
            TextField(
                value = searchTerm ?: lastSearchTerm,
                onValueChange = updateSearchTerm,
                modifier = Modifier
                    .fillMaxWidth()
                    .focusRequester(focusRequester),
                label = { Text(stringResource(R.string.contact_search_action)) }
            )

            if (searchTerm != null) {
                lastSearchTerm = searchTerm

                BackHandler { updateSearchTerm(null) }
            }

            if (searchTerm == "") {
                LaunchedEffect(null) { focusRequester.requestFocus() }
            }
        }

        LazyColumn(
            state = if (list.itemCount > 0) listState else rememberLazyListState(),
            contentPadding = PaddingValuesBottom(contentPadding)
        ) {
            if (lastSyncWarning != null) {
                item {
                    Card(
                        modifier = Modifier.padding(8.dp)
                    ) {
                        Column(
                            modifier = Modifier.padding(8.dp),
                            verticalArrangement = Arrangement.spacedBy(8.dp)
                        ) {
                            Text(
                                text = stringResource(R.string.last_sync_long_ago_title),
                                style = MaterialTheme.typography.headlineMedium
                            )

                            Text(
                                text = stringResource(R.string.last_sync_long_ago_text)
                            )

                            Text(
                                text = stringResource(R.string.last_sync_info, lastSyncWarning)
                            )
                        }
                    }
                }
            }

            items(
                count = list.itemCount,
                key = list.itemKey { it.id }
            ) { index ->
                val item = list[index]

                ContactListItemView(
                    if (item != null) ContactListItem.EntityItem(
                        item = item,
                        onClick = { onItemClicked(item) },
                        now = localDate
                    )
                    else ContactListItem.Placeholder
                )
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyItemScope.ContactListItemView(
    item: ContactListItem
) {
    val placeholderColor = MaterialTheme.colorScheme.onBackground.copy(alpha = .2f)

    Row (
        modifier = Modifier
            .animateItemPlacement()
            .fillMaxWidth()
            .then(
                when (item) {
                    is ContactListItem.Placeholder -> Modifier
                    is ContactListItem.EntityItem -> Modifier.clickable(
                        onClickLabel = item.item.computedDisplayName,
                        role = Role.Button,
                        onClick = item.onClick
                    )
                }
            )
            .padding(PaddingValues(horizontal = 16.dp, vertical = 12.dp)),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        when (item) {
            is ContactListItem.Placeholder -> Box(
                modifier = Modifier
                    .size(32.dp)
                    .background(placeholderColor)
            )
            is ContactListItem.EntityItem -> {
                val presentation = EntityTypePresentation.fromType(item.item.type, LocalContext.current)

                Icon(
                    imageVector = presentation.icon,
                    contentDescription = presentation.label,
                    modifier = Modifier.size(32.dp)
                )
            }
        }

        Text(
            text = when (item) {
                is ContactListItem.Placeholder -> ""
                is ContactListItem.EntityItem -> item.item.computedDisplayName
            },
            style = MaterialTheme.typography.titleLarge,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            color = when (item) {
                is ContactListItem.Placeholder -> Color.Transparent
                is ContactListItem.EntityItem -> Color.Unspecified
            },
            modifier = Modifier
                .weight(1f)
                .then(
                    when (item) {
                        is ContactListItem.Placeholder -> Modifier.background(placeholderColor)
                        is ContactListItem.EntityItem -> Modifier
                    }
                )
        )

        if (item.hasBirthday) {
            Icon(
                imageVector = Icons.Filled.Cake,
                contentDescription = stringResource(R.string.contacts_has_birthday),
                modifier = Modifier.size(24.dp)
            )
        }
    }
}

sealed class ContactListItem {
    abstract val hasBirthday: Boolean

    data class EntityItem(
        val item: Entity,
        val onClick: () -> Unit,
        private val now: LocalDate
    ) : ContactListItem() {
        override val hasBirthday: Boolean = item.personBirthdate != null && now.hasSameDateWithinYear(item.personBirthdate)
    }

    object Placeholder: ContactListItem() {
        override val hasBirthday: Boolean = false
    }
}