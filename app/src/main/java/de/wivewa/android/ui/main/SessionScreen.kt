/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.main

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.ViewGroup
import android.webkit.*
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.SaverScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import de.wivewa.android.R
import de.wivewa.android.network.DeviceAuthentication

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun SessionScreen(
    state: MainActivityModel.State.SessionScreen,
    model: WebViewModel,
    endSession: () -> Unit,
    launchSettings: () -> Unit,
    openExternalUrl: (Uri) -> Unit
) {
    val context = LocalContext.current

    var canGoBack by remember { mutableStateOf(false) }
    val currentDialogLive by model.dialog.collectAsState(initial = null)
    var webviewError by rememberSaveable { mutableStateOf(WebviewErrorState.Working) }
    val webviewState = rememberSaveable (saver = object: Saver<WebView, Bundle> {
        override fun SaverScope.save(value: WebView): Bundle = Bundle().also {
            value.saveState(it)
        }

        override fun restore(value: Bundle): WebView = WebView(context).also {
            it.restoreState(value)
        }
    }) {
        WebView(context).also {
            it.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
    }

    AndroidView(
        modifier = Modifier.fillMaxSize(),
        factory = {
            webviewState.also { webview ->
                webview.settings.javaScriptEnabled = true
                webview.settings.safeBrowsingEnabled = false

                webview.webChromeClient = model.client

                webview.webViewClient = object: WebViewClient() {
                    var lastStartedPage: String? = null

                    override fun onReceivedClientCertRequest(view: WebView, request: ClientCertRequest) {
                        if (request.host == state.domain && request.port == 443) {
                            DeviceAuthentication.handle(request)
                        } else request.ignore()
                    }

                    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                        val matchesHost =
                            request.url.scheme == "https" &&
                                    request.url.host == state.domain &&
                                    request.url.port == -1

                        if (matchesHost) return false
                        else {
                            openExternalUrl(request.url)

                            return true
                        }
                    }

                    override fun onPageCommitVisible(view: WebView?, url: String?) {
                        super.onPageCommitVisible(view, url)

                        canGoBack = webview.canGoBack()
                    }

                    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                        super.onPageStarted(view, url, favicon)

                        lastStartedPage = url
                    }

                    override fun onPageFinished(view: WebView?, url: String?) {
                        super.onPageFinished(view, url)

                        if (webviewError == WebviewErrorState.ErrorRetry) {
                            webviewError = WebviewErrorState.Working
                        }
                    }

                    override fun onReceivedError(
                        view: WebView,
                        request: WebResourceRequest,
                        error: WebResourceError
                    ) {
                        super.onReceivedError(view, request, error)

                        if (request.url.toString() == lastStartedPage) {
                            webviewError = WebviewErrorState.ErrorScreen
                        }
                    }
                }

                webview.addJavascriptInterface(object {
                    @JavascriptInterface
                    fun reportSessionEnd() {
                        endSession.invoke()
                    }

                    @JavascriptInterface
                    fun launchSettings() {
                        launchSettings.invoke()
                    }
                }, "wivewanative")

                canGoBack = webview.canGoBack()

                if (webview.url == null) webview.loadUrl(state.startUrl)
            }
        }
    )

    val currentDialog = currentDialogLive

    if (currentDialog != null) {
        AlertDialog(
            onDismissRequest = {
                when (currentDialog) {
                    is WebViewModel.Dialog.Alert -> currentDialog.ok()
                    is WebViewModel.Dialog.Confirm -> currentDialog.cancel()
                }
            },
            text = { Text(when (currentDialog) {
                is WebViewModel.Dialog.Alert -> currentDialog.message
                is WebViewModel.Dialog.Confirm -> currentDialog.message
            }) },
            confirmButton = {
                TextButton(onClick = { currentDialog.ok() }) {
                    Text(stringResource(android.R.string.ok))
                }
            },
            dismissButton = {
                when (currentDialog) {
                    is WebViewModel.Dialog.Alert -> {/* empty */}
                    is WebViewModel.Dialog.Confirm -> TextButton(onClick = { currentDialog.cancel() }) {
                        Text(stringResource(android.R.string.cancel))
                    }
                }
            }
        )
    }

    if (webviewError != WebviewErrorState.Working) {
        Surface(
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                modifier = Modifier.padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically)
            ) {
                Text(stringResource(R.string.connect_error_text))

                Button(
                    onClick = {
                        webviewError = WebviewErrorState.ErrorRetry
                        webviewState.reload()
                    },
                    enabled = webviewError == WebviewErrorState.ErrorScreen
                ) {
                    Text(stringResource(R.string.connect_error_action))
                }
            }
        }
    }

    BackHandler(canGoBack) {
        if (webviewError != WebviewErrorState.Working) {
            webviewError = WebviewErrorState.ErrorRetry
        }

        webviewState.goBack()
    }
}