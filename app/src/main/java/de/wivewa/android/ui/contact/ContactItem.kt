/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 - 2025 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import de.wivewa.android.R
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo
import de.wivewa.android.database.model.join.EntityWithSalutationAndGroup
import de.wivewa.android.database.model.join.RelatedEntityWithName
import de.wivewa.dialer.ui.PaddingValueSum
import java.time.LocalDate

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun ContactItemScreen(
    screen: ContactsModel.Screen.ContactItem,
    executeCommand: (ContactItemCommand) -> Unit
) {
    BackHandler(enabled = true, onBack = screen.goBack)

    Scaffold (
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.primaryContainer),
                title = {
                    Column {
                        Text(
                            screen.entity.computedDisplayName,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )

                        val subtitleElements = remember {
                            listOfNotNull(screen.entity.personSalutation, screen.entity.personTitle).joinToString(separator = " ")
                        }

                        if (subtitleElements.isNotEmpty()) {
                            Text(
                                subtitleElements,
                                style = MaterialTheme.typography.labelLarge,
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        }
                    }
                },
                navigationIcon = {
                    IconButton(
                        onClick = { screen.goBack() }
                    ) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = stringResource(R.string.topbar_back)
                        )
                    }
                },
                actions = {
                    IconButton(onClick = {
                        executeCommand(ContactItemCommand.LaunchExternal(screen.entity))
                    }) {
                        Icon(
                            Icons.Default.OpenInNew,
                            stringResource(R.string.contact_action_external)
                        )
                    }
                }
            )
        },
        snackbarHost = { SnackbarHost(screen.snackbarHostState) }
    ) { contentPadding ->
        ContactItemScreenContent(
            entity = screen.entity,
            contactInfo = screen.contactInfo,
            addresses = screen.addresses,
            relatedEntities = screen.relatedEntities,
            localDate = screen.localDate,
            bottomSheet = screen.bottomSheet,
            contentPadding = contentPadding,
            executeCommand = executeCommand,
            showDetails = screen.showDetails
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContactItemScreenContent(
    entity: EntityWithSalutationAndGroup,
    contactInfo: List<ContactInfoWithTypeInfo>,
    addresses: List<AddressWithTypeInfo>,
    relatedEntities: List<RelatedEntityWithName>,
    localDate: LocalDate,
    bottomSheet: ContactsModel.Screen.ContactItem.BottomSheet?,
    contentPadding: PaddingValues,
    executeCommand: (ContactItemCommand) -> Unit,
    showDetails: (RelatedEntityWithName) -> Unit
) {
    val mainItems = listOf(
        ContactItemScreenItem.forEntity(
            entity = entity,
            localDate = localDate,
            context = LocalContext.current
        ),
        ContactItemScreenItem.forInfoAndAddress(
            contactInfo = contactInfo,
            address = addresses,
            executeCommand = executeCommand,
            context = LocalContext.current
        ),
        ContactItemScreenItem.forRelatedEntities(
            entities = relatedEntities,
            showDetails = showDetails,
            context = LocalContext.current
        )
    ).flatten()

    if (mainItems.isNotEmpty()) {
        LazyColumn (
            verticalArrangement = Arrangement.spacedBy(8.dp),
            contentPadding = PaddingValueSum(
                PaddingValues(8.dp, 8.dp),
                contentPadding
            )
        ) {
            items(mainItems) {
                ContactDetailCard(
                    item = it,
                    copyToClipboard = executeCommand
                )
            }
        }
    } else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(contentPadding)
                .padding(16.dp),
            verticalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterVertically)
        ) {
            Text(
                stringResource(R.string.contact_empty_title),
                style = MaterialTheme.typography.headlineMedium,
                textAlign = TextAlign.Center
            )

            Text(
                stringResource(R.string.contact_empty_text),
                textAlign = TextAlign.Center
            )
        }
    }

    if (bottomSheet != null) {
        ModalBottomSheet(
            onDismissRequest = bottomSheet.close,
            dragHandle = {}
        ) {
            val bottomItems =
                listOf(
                    ContactItemScreenItem.forRelatedEntity(
                        entity = bottomSheet.entity,
                        showDetails = showDetails,
                        context = LocalContext.current
                    )
                ) + ContactItemScreenItem.forInfoAndAddress(
                    contactInfo = bottomSheet.contactInfo,
                    address = bottomSheet.addresses,
                    executeCommand = executeCommand,
                    context = LocalContext.current
                )

            LazyColumn (
                verticalArrangement = Arrangement.spacedBy(16.dp),
                contentPadding = PaddingValues(top = 24.dp, bottom = 24.dp, start = 16.dp, end = 16.dp)
            ) {
                items (bottomItems) {
                    ContactDetailCard(
                        item = it,
                        copyToClipboard = executeCommand
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ContactDetailCard(
    item: ContactItemScreenItem,
    copyToClipboard: (ContactItemCommand.CopyToClipboard) -> Unit
) {
    var showMenu by remember { mutableStateOf(false) }

    Card (
        modifier = Modifier
            .fillMaxWidth()
            .then(
                if (item.action != null)
                    Modifier.combinedClickable(
                        enabled = true,
                        onClickLabel = item.action.label,
                        role = Role.Button,
                        onClick = item.action.handler,
                        onLongClick = { showMenu = true }
                    )
                else Modifier.clickable(
                    enabled = true,
                    onClickLabel = stringResource(R.string.generic_open_menu),
                    role = Role.Button,
                    onClick = { showMenu = true }
                )
            ),
        colors = when (item.highlight) {
            false -> CardDefaults.cardColors()
            true -> CardDefaults.cardColors(containerColor = MaterialTheme.colorScheme.secondaryContainer)
        }
    ) {
        DropdownMenu(
            expanded = showMenu,
            onDismissRequest = { showMenu = false }
        ) {
            DropdownMenuItem(
                text = {
                    Text(stringResource(R.string.generic_copy_to_clipboard))
                },
                onClick = {
                    copyToClipboard(
                        ContactItemCommand.CopyToClipboard(
                            item.typeLabel, item.text
                        )
                    )

                    showMenu = false
                }
            )
        }

        Row (
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(12.dp)
        ) {
            Column (
                modifier = Modifier.weight(1f)
            ) {
                Text(
                    item.text,
                    style = MaterialTheme.typography.titleLarge
                )

                val visibleType = if (item.hideType) null else item.typeLabel

                if (visibleType != null || item.subtext != null) {
                    Text(
                        listOfNotNull(visibleType, item.subtext).joinToString(separator = ", "),
                        style = MaterialTheme.typography.labelLarge
                    )
                }
            }

            if (item.icon != null) {
                Icon(
                    imageVector = item.icon,
                    contentDescription = item.typeLabel,
                    modifier = Modifier
                        .padding(0.dp, 8.dp)
                )
            }
        }
    }
}