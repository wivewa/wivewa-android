/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.contact

import android.content.Context
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cake
import androidx.compose.material.icons.filled.Group
import androidx.compose.material.icons.filled.Map
import androidx.compose.ui.graphics.vector.ImageVector
import de.wivewa.android.R
import de.wivewa.android.database.model.ContactInfo
import de.wivewa.android.database.model.join.AddressWithTypeInfo
import de.wivewa.android.database.model.join.ContactInfoWithTypeInfo
import de.wivewa.android.database.model.join.EntityWithSalutationAndGroup
import de.wivewa.android.database.model.join.RelatedEntityWithName
import de.wivewa.android.extensions.hasSameDateWithinYear
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

data class ContactItemScreenItem(
    val typeLabel: String,
    val icon: ImageVector?,
    val text: String,
    val subtext: String? = null,
    val highlight: Boolean = false,
    val hideType: Boolean = false,
    val action: ContactItemScreenAction?
) {
    companion object {
        fun forEntity(
            entity: EntityWithSalutationAndGroup,
            localDate: LocalDate,
            context: Context
        ): List<ContactItemScreenItem> {
            val groupNameItem = if (entity.groupName != null) {
                ContactItemScreenItem(
                    typeLabel = context.getString(R.string.contact_group),
                    icon = Icons.Filled.Group,
                    text = entity.groupName,
                    action = null
                )
            } else null

            val birthdateItem = if (entity.personBirthdate != null) {
                val now = localDate
                val birth = entity.personBirthdate

                val hasBirthday = now.hasSameDateWithinYear(birth)

                ContactItemScreenItem(
                    typeLabel = context.getString(R.string.contact_birthdate),
                    icon = Icons.Filled.Cake,
                    text = entity.personBirthdate.format(
                        DateTimeFormatter.ofLocalizedDate(
                            FormatStyle.MEDIUM)),
                    highlight = hasBirthday,
                    action = null
                )
            } else null

            return listOfNotNull(groupNameItem, birthdateItem)
        }

        fun forInfoAndAddress(
            contactInfo: List<ContactInfoWithTypeInfo>,
            address: List<AddressWithTypeInfo>,
            executeCommand: (ContactItemCommand) -> Unit,
            context: Context
        ): List<ContactItemScreenItem> {
            val contactInfoSections = contactInfo.groupBy { it.type }
            val info = contactInfoSections[ContactInfo.TYPE_CONTACT] ?: emptyList()
            val relation = contactInfoSections[ContactInfo.TYPE_RELATION] ?: emptyList()

            fun mapInfo(info: ContactInfoWithTypeInfo): ContactItemScreenItem {
                val (text, subtext) = ContactExtraInfo.forContactInfo(info)

                return ContactItemScreenItem(
                    typeLabel = info.contactTypeName,
                    icon = ContactDetailCardIcons.forInfo(info),
                    text = text,
                    subtext = subtext,
                    action = ContactItemScreenAction.forInfo(info, executeCommand, context)
                )
            }

            fun mapAddress(address: AddressWithTypeInfo): ContactItemScreenItem {
                val (text, subtext) = ContactExtraInfo.forAddress(address)

                return ContactItemScreenItem(
                    typeLabel = address.contactTypeName,
                    icon = Icons.Filled.Map,
                    text = text,
                    subtext = subtext,
                    action = ContactItemScreenAction.forAddress(address, executeCommand, context)
                )
            }

            val infoItems = info.map(::mapInfo)
            val addressItems = address.map(::mapAddress)
            val relationItems = relation.map(::mapInfo)

            return listOf(infoItems, addressItems, relationItems).flatten()
        }

        fun forRelatedEntity(
            entity: RelatedEntityWithName,
            showDetails: (RelatedEntityWithName) -> Unit,
            context: Context
        ): ContactItemScreenItem {
            val presentation = EntityTypePresentation.fromType(entity.entity2Type, context)

            return ContactItemScreenItem(
                typeLabel = presentation.label,
                icon = presentation.icon,
                text = entity.entity2Name,
                hideType = true,
                action = ContactItemScreenAction(
                    label = context.getString(R.string.contact_action_details),
                    handler = { showDetails(entity) }
                ),
            )
        }

        fun forRelatedEntities(
            entities: List<RelatedEntityWithName>,
            showDetails: (RelatedEntityWithName) -> Unit,
            context: Context
        ): List<ContactItemScreenItem> = entities.map {
            forRelatedEntity(it, showDetails, context)
        }
    }
}