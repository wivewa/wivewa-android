/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.network

import android.os.Parcelable
import android.webkit.CookieManager
import de.wivewa.android.database.model.*
import kotlinx.parcelize.Parcelize

interface ServerApi {
    suspend fun performClientLogin(
        cookieManager: CookieManager,
        customerId: Long?,
        route: ClientLoginRoute?
    ): ClientLoginResponse
    suspend fun getCustomerInfo(): CustomerInfoResponse
    suspend fun getAddressbook(): AddressbookResponse
    suspend fun uploadPhoneCalls(customerId: Long, calls: List<UploadPhoneCall>)
    suspend fun getLastTask(entityIds: Set<Long>): TaskInfo?
}

interface ServerApiFactory {
    fun create(domain: String): ServerApi
}

sealed class ClientLoginRoute: Parcelable {
    @Parcelize
    data class EntityDetails(val entityId: Long, val tab: Tab?): ClientLoginRoute() {
        enum class Tab {
            Calls
        }
    }
    @Parcelize
    data class TaskDetails(val taskId: Long): ClientLoginRoute()
}

sealed class ClientLoginResponse {
    data class Success(val initialPath: String?): ClientLoginResponse()
    object UnknownCert: ClientLoginResponse()
    data class Other(val code: String): ClientLoginResponse()
}

typealias CustomerInfoResponse = List<CustomerInfoResponseItem>

data class CustomerInfoResponseItem(
    val customerId: Long,
    val customerName: String,
    val roles: Set<String>
)

data class AddressbookResponse(
    val customers: List<Customer>,
    val contactTypes: List<ContactType>,
    val salutations: List<Salutation>,
    val entityGroups: List<EntityGroup>,
    val entities: List<Entity>,
    val addresses: List<Address>,
    val contactInfos: List<ContactInfo>,
    val relatedEntity: List<RelatedEntity>
)

data class UploadPhoneCall(
    val id: String,
    val kind: String,
    val from: String,
    val to: String,
    val start: Long,
    val wait: Long,
    val length: Long,
    val status: String
)

data class TaskInfo(val id: Long, val title: String, val ownerId: Long)