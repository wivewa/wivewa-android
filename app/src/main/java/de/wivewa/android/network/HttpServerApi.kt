/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.network

import android.util.JsonReader
import android.webkit.CookieManager
import de.wivewa.android.database.model.*
import de.wivewa.android.extensions.assertSuccess
import de.wivewa.android.extensions.enqueue
import de.wivewa.android.extensions.postJson
import de.wivewa.android.extensions.readList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke
import okhttp3.OkHttpClient
import okhttp3.Request
import okio.IOException
import java.security.KeyStore
import javax.net.ssl.*
import kotlin.coroutines.suspendCoroutine


class HttpServerApi(domain: String): ServerApi {
    companion object {
        private val plainOkHttpClient by lazy {
            OkHttpClient.Builder()
                .build()
        }

        private val okHttpClient by lazy {
            val keyManager = DeviceAuthentication.getClientKeyManager()

            val socketFactory = SSLContext.getInstance("TLS").also {
                it.init(arrayOf(keyManager), null, null)
            }.socketFactory

            val trustManager = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).also {
                it.init(null as KeyStore?)
            }.trustManagers.single()

            plainOkHttpClient.newBuilder()
                .sslSocketFactory(socketFactory, trustManager as X509TrustManager)
                .build()
        }

        private const val OK = "ok"
        private const val CODE = "code"
        private const val UNKNOWNCERT = "unknowncert"
        private const val CUSTOMER = "customer"
        private const val ID = "id"
        private const val NAME = "name"
        private const val ROLES = "roles"
        private const val CUSTOMER_ID = "customerId"
        private const val CUSTOMER_ID2 = "customer_id"
        private const val CALLS = "calls"
        private const val FROM = "from"
        private const val TO = "to"
        private const val KIND = "kind"
        private const val START = "start"
        private const val WAIT = "wait"
        private const val LENGTH = "length"
        private const val STATUS = "status"
        private const val CUSTOMERS = "customers"
        private const val CONTACT_TYPES = "contact_types"
        private const val SALUTATIONS = "salutations"
        private const val ENTITY_GROUPS = "entity_groups"
        private const val ENTITIES = "entities"
        private const val ADDRESSES = "addresses"
        private const val CONTACTINFOS = "contactinfos"
        private const val RELATED_ENTITIES = "related_entities"
        private const val ROUTE = "route"
        private const val TYPE = "type"
        private const val TASK = "task"
        private const val TASK_ID = "task_id"
        private const val PATH = "path"
        private const val ENTITY = "entity"
        private const val ENTITY_ID = "entity_id"
        private const val ENTITY_IDS = "entity_ids"
        private const val TAB = "tab"
        private const val TASK_INFO = "task_info"
        private const val TITLE = "title"
        private const val OWNER_ID = "owner_id"
    }

    object Factory: ServerApiFactory {
        override fun create(domain: String): ServerApi = HttpServerApi(domain)
    }

    private val baseUrl = "https://$domain"

    override suspend fun performClientLogin(
        cookieManager: CookieManager,
        customerId: Long?,
        route: ClientLoginRoute?
    ): ClientLoginResponse = Dispatchers.IO.invoke {
        Request.Builder()
            .url("$baseUrl/api/app/login")
            .postJson { writer ->
                writer.beginObject()

                if (customerId != null) writer.name(CUSTOMER_ID2).value(customerId)

                if (route != null) {
                    writer.name(ROUTE).beginObject()

                    when (route) {
                        is ClientLoginRoute.EntityDetails -> {
                            writer.name(TYPE).value(ENTITY)
                            writer.name(ENTITY_ID).value(route.entityId)

                            if (route.tab != null) when (route.tab) {
                                ClientLoginRoute.EntityDetails.Tab.Calls -> writer.name(TAB).value(CALLS)
                            }
                        }
                        is ClientLoginRoute.TaskDetails -> writer.name(TYPE).value(TASK).name(TASK_ID).value(route.taskId)
                    }

                    writer.endObject()
                }

                writer.endObject()
            }
            .addHeader("X-Nativeclient", "yes")
            .also { builder ->
                cookieManager.getCookie(baseUrl)?.also { cookie ->
                    builder.addHeader("Cookie", cookie)
                }
            }
            .build()
            .let { okHttpClient.newCall(it) }
            .enqueue()
            .use { response ->
                response.assertSuccess()

                Dispatchers.Main.invoke {
                    for (cookie in response.headers("Set-Cookie")) {
                        suspendCoroutine<Boolean> { continuation ->
                            cookieManager.setCookie(baseUrl, cookie) { result ->
                                continuation.resumeWith(Result.success(result))
                            }
                        }
                    }
                }

                var ok: Boolean? = null
                var code: String? = null
                var path: String? = null

                JsonReader(response.body!!.charStream()).use { reader ->
                    reader.beginObject()
                    while (reader.hasNext()) {
                        when (reader.nextName()) {
                            OK -> ok = reader.nextBoolean()
                            CODE -> code = reader.nextString()
                            PATH -> path = reader.nextString()
                            else -> reader.skipValue()
                        }
                    }
                    reader.endObject()
                }

                run {
                    val ok = ok
                    val code = code

                    if (ok == null) throw MalformedResponseException()
                    else if (ok == true) ClientLoginResponse.Success(path)
                    else if (code == null) throw MalformedResponseException()
                    else if (code == UNKNOWNCERT) ClientLoginResponse.UnknownCert
                    else ClientLoginResponse.Other(code)
                }
            }
    }

    override suspend fun getCustomerInfo(): CustomerInfoResponse = Dispatchers.IO.invoke {
        Request.Builder()
            .url("$baseUrl/api/app/customer-info")
            .addHeader("X-Nativeclient", "yes")
            .build()
            .let { okHttpClient.newCall(it) }
            .enqueue()
            .use { response ->
                response.assertSuccess()

                JsonReader(response.body!!.charStream()).use { reader ->
                    mutableListOf<CustomerInfoResponseItem>().also { list ->
                        reader.beginArray()
                        while (reader.hasNext()) {
                            var customerId: Long? = null
                            var customerName: String? = null
                            var roles: Set<String>? = null

                            reader.beginObject()
                            while (reader.hasNext()) {
                                when (reader.nextName()) {
                                    CUSTOMER -> {
                                        reader.beginObject()
                                        while (reader.hasNext()) {
                                            when (reader.nextName()) {
                                                ID -> customerId = reader.nextLong()
                                                NAME -> customerName = reader.nextString()
                                                else -> reader.skipValue()
                                            }
                                        }
                                        reader.endObject()
                                    }
                                    ROLES -> roles = mutableSetOf<String>().also { roleList ->
                                        reader.beginArray()
                                        while (reader.hasNext()) roleList.add(reader.nextString())
                                        reader.endArray()
                                    }
                                    else -> reader.skipValue()
                                }
                            }
                            reader.endObject()

                            list.add(CustomerInfoResponseItem(
                                customerId = customerId!!,
                                customerName = customerName!!,
                                roles = roles!!
                            ))
                        }
                        reader.endArray()
                    }
                }
            }
    }

    override suspend fun getAddressbook(): AddressbookResponse = Dispatchers.IO.invoke {
        Request.Builder()
            .url("$baseUrl/api/app/addressbook/download")
            .addHeader("X-Nativeclient", "yes")
            .build()
            .let { okHttpClient.newCall(it) }
            .enqueue()
            .use { response ->
                response.assertSuccess()

                JsonReader(response.body!!.charStream()).use { reader ->
                    var customers: List<Customer>? = null
                    var contactTypes: List<ContactType>? = null
                    var salutations: List<Salutation>? = null
                    var entityGroups: List<EntityGroup>? = null
                    var entities: List<Entity>? = null
                    var addresses: List<Address>? = null
                    var contactInfos: List<ContactInfo>? = null
                    var relatedEntity: List<RelatedEntity>? = null

                    reader.beginObject()
                    while (reader.hasNext()) {
                        when (reader.nextName()) {
                            CUSTOMERS -> customers = reader.readList(Customer.Companion::fromJson)
                            CONTACT_TYPES -> contactTypes = reader.readList(ContactType.Companion::fromJson)
                            SALUTATIONS -> salutations = reader.readList(Salutation.Companion::fromJson)
                            ENTITIES -> entities = reader.readList(Entity.Companion::fromJson)
                            ENTITY_GROUPS -> entityGroups = reader.readList(EntityGroup.Companion::fromJson)
                            ADDRESSES -> addresses = reader.readList(Address.Companion::fromJson)
                            CONTACTINFOS -> contactInfos = reader.readList(ContactInfo.Companion::fromJson)
                            RELATED_ENTITIES -> relatedEntity = reader.readList(RelatedEntity.Companion::fromJson)
                            else -> reader.skipValue()
                        }
                    }
                    reader.endObject()

                    AddressbookResponse(
                        customers = customers!!,
                        contactTypes = contactTypes!!,
                        salutations = salutations!!,
                        entityGroups = entityGroups!!,
                        entities = entities!!,
                        addresses = addresses!!,
                        contactInfos = contactInfos!!,
                        relatedEntity = relatedEntity!!
                    )
                }
            }
    }

    override suspend fun uploadPhoneCalls(customerId: Long, calls: List<UploadPhoneCall>): Unit = Dispatchers.IO.invoke {
        Request.Builder()
            .url("$baseUrl/api/app/phone/upload-calls")
            .addHeader("X-Nativeclient", "yes")
            .postJson { writer ->
                writer.beginObject()

                writer.name(CUSTOMER_ID).value(customerId)

                writer.name(CALLS).beginArray()
                for (call in calls) {
                    writer.beginObject()

                    writer.name(ID).value(call.id)
                    writer.name(KIND).value(call.kind)
                    writer.name(FROM).value(call.from)
                    writer.name(TO).value(call.to)
                    writer.name(START).value(call.start)
                    writer.name(WAIT).value(call.wait)
                    writer.name(LENGTH).value(call.length)
                    writer.name(STATUS).value(call.status)

                    writer.endObject()
                }; writer.endArray()

                writer.endObject()
            }
            .build()
            .let { okHttpClient.newCall(it) }
            .enqueue()
            .use { response ->
                response.assertSuccess()
            }
    }

    override suspend fun getLastTask(entityIds: Set<Long>): TaskInfo?  = Dispatchers.IO.invoke {
        Request.Builder()
            .url("$baseUrl/api/app/taskplaner/last-task")
            .addHeader("X-Nativeclient", "yes")
            .postJson { writer ->
                writer.beginObject()

                writer.name(ENTITY_IDS).beginArray()
                entityIds.forEach { writer.value(it) }
                writer.endArray()

                writer.endObject()
            }
            .build()
            .let { okHttpClient.newCall(it) }
            .enqueue()
            .use { response ->
                response.assertSuccess()

                JsonReader(response.body!!.charStream()).use { reader ->
                    var task: TaskInfo? = null

                    reader.beginObject()
                    while (reader.hasNext()) when (reader.nextName()) {
                        TASK_INFO -> {
                            var id: Long? = null
                            var title: String? = null
                            var ownerId: Long? = null

                            reader.beginObject()
                            while (reader.hasNext()) when (reader.nextName()) {
                                ID -> id = reader.nextLong()
                                TITLE -> title = reader.nextString()
                                OWNER_ID -> ownerId = reader.nextLong()
                                else -> reader.skipValue()
                            }
                            reader.endObject()

                            task = TaskInfo(
                                id = id!!,
                                title = title!!,
                                ownerId = ownerId!!
                            )
                        }
                        else -> reader.skipValue()
                    }
                    reader.endObject()

                    task
                }
            }
    }

    class MalformedResponseException: IOException("malformed response")
}