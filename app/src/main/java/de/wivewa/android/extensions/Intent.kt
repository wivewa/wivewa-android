/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.extensions

import android.content.Intent
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.os.Parcelable

inline fun <reified T: Parcelable> Intent.getParcelableExtraCompat(name: String): T? =
    if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) getParcelableExtra(name, T::class.java)
    else getParcelableExtra(name)