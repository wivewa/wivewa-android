/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android

import android.Manifest
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.telecom.Call
import android.telecom.TelecomManager
import android.util.Log
import android.widget.RemoteViews
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.work.WorkManager
import com.google.i18n.phonenumbers.PhoneNumberUtil
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.database.Database
import de.wivewa.android.database.model.*
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.client.DialerNotificationListenerService
import de.wivewa.android.network.HttpServerApi
import de.wivewa.android.network.ServerApiFactory
import de.wivewa.android.ui.contact.ContactExtraInfo
import de.wivewa.android.utils.PendingIntents
import de.wivewa.android.worker.SyncPhoneCallsWorker
import kotlinx.coroutines.*
import java.lang.Runnable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class ListenerService : DialerNotificationListenerService() {
    companion object {
        private const val LOG_TAG = "ListenerService"

        private val handler = Handler(Looper.getMainLooper())
    }

    private lateinit var executor: ExecutorService
    private lateinit var database: Database
    private lateinit var workManager: WorkManager
    private lateinit var telecomManager: TelecomManager
    private var stopped = false
    private var debug = false

    private val apiFactory: ServerApiFactory = HttpServerApi.Factory
    private val scope = CoroutineScope(Dispatchers.Main + Job())
    private val ongoingCallIds = mutableSetOf<String>()
    private val updateRunnable = Runnable { if (!stopped) cleanupAsync() }
    private val phoneNumberUtil by lazy { PhoneNumberUtil.getInstance() }

    override fun onCreate() {
        super.onCreate()

        executor = Executors.newSingleThreadExecutor()
        database = AppDatabase.with(this)
        workManager = WorkManager.getInstance(this)
        telecomManager = getSystemService()!!
        debug = applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE != 0
    }

    override fun onDestroy() {
        super.onDestroy()

        stopped = true
        executor.shutdown()
        scope.cancel()
        handler.removeCallbacks(updateRunnable)
    }

    override fun onCallUpdated(callInfo: CallInfo) {
        val now = System.currentTimeMillis()

        val incoming = when(callInfo.direction) {
            Call.Details.DIRECTION_INCOMING -> true
            Call.Details.DIRECTION_OUTGOING -> false
            else -> return
        }

        executor.submit {
            val remoteNumber =
                if (callInfo.handlePresentation == TelecomManager.PRESENTATION_ALLOWED)
                    parseNumber(callInfo.remoteHandle) ?: ""
                else
                    ""

            database.runInTransaction {
                val isActiveCall = callInfo.state != Call.STATE_DISCONNECTED
                val enableCallerInfo = database.configuration.getValue(ConfigurationItem.PHONE_CALLER_INFO) == ConfigurationItem.Bool.YES

                if (!isActiveCall) ongoingCallIds.remove(callInfo.id)
                else if (ongoingCallIds.add(callInfo.id)) {
                    if (remoteNumber.isNotEmpty()) {
                        val matchingContacts =
                            if (enableCallerInfo) database.contactInfo.getByUriSchema(ContactType.UriSchema.TEL)
                                .map {
                                    val match = when (phoneNumberUtil.isNumberMatch(remoteNumber, it.info)) {
                                        PhoneNumberUtil.MatchType.EXACT_MATCH -> 3
                                        PhoneNumberUtil.MatchType.NSN_MATCH -> 2
                                        else -> 0
                                    }

                                    Pair(it, match)
                                }
                                .filter { it.second > 0 }
                            else emptyList()

                        if (matchingContacts.isNotEmpty()) {
                            val bestMatchType = matchingContacts.maxBy { it.second }.second
                            val bestMatchingContactInfoIds = matchingContacts
                                .filter { it.second == bestMatchType }
                                .map { it.first.id }

                            val bestMatchingContacts = database.entity.getByContactInfoIdsSync(bestMatchingContactInfoIds)

                            val firstContact = bestMatchingContacts.first()

                            val entityIds = bestMatchingContacts.map { it.id }
                            val entityNames = bestMatchingContacts
                                .map { it.computedDisplayName }
                                .toSortedSet()
                                .joinToString(separator = ", ")

                            val callerIntent = PendingIntents.launchContact(this, firstContact.id)

                            val domain = database.configuration.getValue(ConfigurationItem.DOMAIN)

                            enhanceCall(callInfo, CallExtras().also {
                                it.displayName = entityNames
                                it.callerDetailsAction = callerIntent
                            })

                            if (domain != null) scope.launch {
                                val network = apiFactory.create(domain)

                                try {
                                    network.getLastTask(entityIds.toSet())?.let { lastTask ->
                                        val taskIntent = PendingIntents.launchTask(
                                            this@ListenerService,
                                            lastTask.ownerId,
                                            lastTask.id
                                        )

                                        val view = RemoteViews(packageName, R.layout.task_remote_view).also {
                                            it.setTextViewText(R.id.task_title, lastTask.title)
                                            it.setOnClickPendingIntent(R.id.task_action, taskIntent)
                                        }

                                        enhanceCall(callInfo, CallExtras().also {
                                            it.callerDetailsView = view
                                        })
                                    }
                                } catch (ex: Exception) {
                                    if (debug) {
                                        Log.d(LOG_TAG, "could not get tasks", ex)
                                    }
                                }
                            }
                        } else {
                            val callOrigin = ContactExtraInfo.forContactInfo(remoteNumber).second

                            if (callOrigin != null) {
                                val view = RemoteViews(packageName, R.layout.call_origin_remote_view).also {
                                    it.setTextViewText(R.id.call_origin, callOrigin)
                                }

                                enhanceCall(callInfo, CallExtras().also {
                                    it.callerDetailsView = view
                                })
                            }
                        }
                    }
                }

                val phoneAccount = callInfo.phoneAccount ?: return@runInTransaction

                val phoneAccountConfiguration = database.phoneAccountUploadConfiguration.getByOsParams(phoneAccount.id, phoneAccount.componentName)
                    ?: return@runInTransaction

                val oldLoggedCall = database.loggedPhoneCall.findById(callInfo.id)

                var loggedCall = oldLoggedCall ?: run {
                    val phoneAccountInfo =
                        if (ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) null
                        else telecomManager.getPhoneAccount(phoneAccount)

                    val localNumber = parseNumber(phoneAccountInfo?.address?.toString() ?: "") ?: ""

                    LoggedPhoneCall(
                        id = callInfo.id,
                        kind = if (incoming) PhoneCallKindStrings.RING else PhoneCallKindStrings.CALL,
                        from = if (incoming) remoteNumber else localNumber,
                        to = if (incoming) localNumber else remoteNumber,
                        start = now,
                        wait = 0,
                        length = 0,
                        status = if (incoming) PhoneCallStatusStrings.RINGING else PhoneCallStatusStrings.DIALING,
                        synced = false,
                        entityId = phoneAccountConfiguration.uploadEntityId
                    ).also { scheduleUpdate() }
                }

                if (loggedCall.status == PhoneCallStatusStrings.DISCONNECTED) return@runInTransaction
                else if (
                    callInfo.state == Call.STATE_ACTIVE &&
                    loggedCall.status != PhoneCallStatusStrings.CONNECTED
                ) {
                    loggedCall = updateTimers(
                        loggedCall,
                        now
                    ).copy(status = PhoneCallStatusStrings.CONNECTED)
                } else if (
                    callInfo.state == Call.STATE_HOLDING &&
                    loggedCall.status == PhoneCallStatusStrings.CONNECTED
                ) {
                    loggedCall =
                        updateTimers(loggedCall, now).copy(status = PhoneCallStatusStrings.HOLD)
                } else if (
                    callInfo.state == Call.STATE_DISCONNECTING ||
                    callInfo.state == Call.STATE_DISCONNECTED
                ) {
                    if (
                        loggedCall.status == PhoneCallStatusStrings.RINGING ||
                        loggedCall.status == PhoneCallStatusStrings.DIALING
                    ) {
                        loggedCall = updateTimers(
                            loggedCall,
                            now
                        ).copy(status = PhoneCallStatusStrings.UNANSWERED)
                    } else if (
                        loggedCall.status == PhoneCallStatusStrings.CONNECTED ||
                        loggedCall.status == PhoneCallStatusStrings.HOLD
                    ) {
                        loggedCall = updateTimers(
                            loggedCall,
                            now
                        ).copy(status = PhoneCallStatusStrings.DISCONNECTED)
                    }
                }

                if (oldLoggedCall == null) {
                    database.loggedPhoneCall.insert(loggedCall.copy(synced = false))

                    enqueueSync()
                } else if (loggedCall != oldLoggedCall) {
                    database.loggedPhoneCall.update(loggedCall.copy(synced = false))

                    enqueueSync()
                }
            }
        }
    }

    override fun onCallEnded(callInfo: CallInfo) {
        callInfo.state = Call.STATE_DISCONNECTED

        onCallUpdated(callInfo)
        cleanupAsync()
    }

    private fun cleanupAsync() {
        val now = System.currentTimeMillis()

        executor.submit {
            val latch = CountDownLatch(1)
            var currentCalls: List<CallInfo>? = null

            getCurrentCalls {
                currentCalls = it.getOrNull()

                latch.countDown()
            }

            latch.await(1, TimeUnit.SECONDS)

            val currentSetCalls = currentCalls ?: return@submit
            val currentCallIds = currentSetCalls.map { it.id }.toSet()

            ongoingCallIds.removeAll { !currentCallIds.contains(it) }

            database.runInTransaction {
                val list = database.loggedPhoneCall.findByStatus(PhoneCallStatusStrings.TYPE_NOT_DONE)

                list.forEach { item ->
                    if (!currentCallIds.contains(item.id)) {
                        database.loggedPhoneCall.update(
                            item.copy(
                                synced = false,
                                status = when (item.status) {
                                    PhoneCallStatusStrings.CONNECTED -> PhoneCallStatusStrings.DISCONNECTED
                                    PhoneCallStatusStrings.HOLD -> PhoneCallStatusStrings.DISCONNECTED
                                    else -> PhoneCallStatusStrings.UNANSWERED
                                }
                            )
                        )
                    } else {
                        database.loggedPhoneCall.update(
                            updateTimers(item, now).copy(synced = false)
                        )
                    }
                }

                if (list.isNotEmpty()) scheduleUpdate()
            }
        }
    }

    private fun scheduleUpdate() {
        if (debug) {
            Log.d(LOG_TAG, "scheduleUpdate()")
        }

        handler.removeCallbacks(updateRunnable)
        handler.postDelayed(updateRunnable, 1000 * 180)
    }

    private fun updateWaitPhase(loggedCall: LoggedPhoneCall, now: Long) = loggedCall.copy(
        wait = (now - loggedCall.end).coerceAtLeast(0).coerceAtLeast(loggedCall.wait)
    )

    private fun updateTalkPhase(loggedCall: LoggedPhoneCall, now: Long) = loggedCall.copy(
        length = (now - loggedCall.end).coerceAtLeast(0).coerceAtLeast(loggedCall.length)
    )

    private fun isTalkPhase(loggedCall: LoggedPhoneCall): Boolean? = when (loggedCall.status) {
        PhoneCallStatusStrings.DIALING -> false
        PhoneCallStatusStrings.RINGING -> false
        PhoneCallStatusStrings.CONNECTED -> true
        PhoneCallStatusStrings.HOLD -> false
        PhoneCallStatusStrings.UNANSWERED -> false
        PhoneCallStatusStrings.DISCONNECTED -> false
        else -> null
    }

    private fun updateTimers(loggedCall: LoggedPhoneCall, now: Long) = when (isTalkPhase(loggedCall)) {
        true -> updateTalkPhase(loggedCall, now)
        false -> updateWaitPhase(loggedCall, now)
        null -> loggedCall
    }

    private fun parseNumber(number: String): String? {
        return if (number.startsWith("tel:")) Uri.parse(number)?.schemeSpecificPart
        else null
    }

    private fun enqueueSync() {
        SyncPhoneCallsWorker.enqueue(workManager)
    }
}