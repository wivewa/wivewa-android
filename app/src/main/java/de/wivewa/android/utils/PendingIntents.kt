/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.utils

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import de.wivewa.android.contants.PendingIntentIds
import de.wivewa.android.network.ClientLoginRoute
import de.wivewa.android.ui.contact.ContactsActivity
import de.wivewa.android.ui.main.MainActivity

object PendingIntents {
    fun launchTask(context: Context, ownerId: Long, taskId: Long): PendingIntent = PendingIntent.getActivity(
        context,
        PendingIntentIds.LAUNCH_TASK,
        MainActivity.buildIntent(
            context,
            ownerId,
            ClientLoginRoute.TaskDetails(taskId)
        )
            .setData(Uri.fromParts("dummy", "$ownerId/$taskId", null))
            .addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_DOCUMENT),
        PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
    )

    fun launchContact(context: Context, id: Long): PendingIntent = PendingIntent.getActivity(
        context,
        PendingIntentIds.LAUNCH_CONTACT,
        ContactsActivity.createIntentForContact(context, id),
        PendingIntent.FLAG_IMMUTABLE
    )
}